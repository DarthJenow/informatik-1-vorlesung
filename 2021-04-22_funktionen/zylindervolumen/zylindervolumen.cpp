/*
 * Dateiname:			zylindervolumen.cpp
 * Datum:				2021-04-22 11:35:20
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-22 11:35:20
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Volumen eiens Zylinders aus Radius und Höhe mittels Funktionen
 * Eingabe:				Radius, Höhe
 * Ausgabe:				Volumen
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double zylinder_volumen(double radius, double hoehe);
double kreis_flaeche(double radius);
double zylinder_volumen2(double radius, double hoehe);
double volumen2(double basis, double hoehe);

/**
 * Deklaration der Funktion names zylinder_volumen mit 
 * Rückgabetyp double --> gibt Ergebnis vom Typ double zurück
 * WICHTIG: eine Funktion kann NUR EIN Ergebnis zurückgeben
 * Funktionsname --> zylinder_volumen
 * Liste der Argumente (Eingabewerte an die Funktion) --> erwartet 2 Argumente je vom Typ double
 * 		Argumente mit Namen radius und hoehe definiert, um später Funktionsverhalten zu beschreiben
 */

/**
 * Funktionsdeklaration allgemein:
 * Rückgabetyp Funktionsname(Argumenttyp1 Argument1, ...);
 */

int main(int argc, char const *argv[]) {
	double radius, hoehe, volumen;

	cout << "Willkommen zum Zylindervolumenrechner!" << endl;
	cout << "Geben Sie die Radius und Hoehe ihres Zylinders ein: ";
	cin >> radius >> hoehe;

	volumen = zylinder_volumen(radius, hoehe);
	/**
	 * Aufruf der Funktion zylinder_volumen, hier in Form einer Zuweisung
	 * volumen bekommt das Ergebnis der Funktionsausführung zugewiesen
	 * beim Aufruf wird hier der Wert von radius in die Variable radius
	 * der Funktion zylinder_volumen kopiert,
	 * hoehe und hoehe analog
	 */

	/**
	 * WICHTIG: der Rückgabewert einer Funktion muss immer irgendwo
	 * "aufgefangen" werden, z.B. in einer Zuweisung, Rechnung, cout, ..
	 */

	cout << "Ein Zylinder mit dem Radius r = " << radius << " und der Hoehe h = " << hoehe << " hat ein Volumen von V = " << volumen << endl;

	cout << "Volumen mit inline-Funktion " << zylinder_volumen(radius, hoehe) << endl;

	cout << "Was passiert hier? " << zylinder_volumen(hoehe, radius) << " [SPOILER: radius und hoehe beim Funktionsaufruf vertauscht]" << endl;
	/**
	 * Hier wird hoehe für radius und radius für hoehe genommen; gibt falsche Werte!
	 */

	cout << "Volumen mit r = 5 und h = 3.5 --> V = " << zylinder_volumen(5, 3.5) << endl;

	cout << "Volument mittels 2er Funktionen: " << zylinder_volumen2(radius, hoehe) << endl;

	cout << "Volumen mittels Funktionsaufruf innerhalb Funktionsaufruf: " << volumen2(kreis_flaeche(radius), hoehe) << endl;

	return 0;
}

double zylinder_volumen(double radius, double hoehe) {
	return radius * radius * M_PI * hoehe;
	/**
	 * sofortiges Beenden der Funktion, Rücksprung zum Aufrufort und Rückgabe des Ergebnisses
	 * Die Rechnung kann auch direkt im return Statement ausgeführt werden
	 */
}

double kreis_flaeche(double radius) {
	return radius * radius * M_PI;
}

double zylinder_volumen2(double radius, double hoehe) {
	return kreis_flaeche(radius) * hoehe;
}

double volumen2(double basis, double hoehe) {
	return basis * hoehe;
}