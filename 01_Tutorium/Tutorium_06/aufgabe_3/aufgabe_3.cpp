/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-05-20 03:24:50
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 03:24:50
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	string input_string;

	cout << "Geben Sie eine String ein: ";
	getline(cin, input_string);

	int obere = 0, untere = 0;

	for (int i = 0; i < input_string.length(); i++) {
		char c = input_string.at(i);

		if (c >= 'A' && c <= 'A' + 13 || c >= 'a' && c <= 'a' + 13) {
			obere++;
		} else if (c != ' ') {
			untere++;
		}
	}

	cout << "Der String " << input_string << " enthaelt " << obere << " Zeichen aus der oberen Haelfte des Alphabets und " << untere << " Zeichen aus der unteren Haelfte des Alphabets und Sonderzeichen" << endl;
	
	return 0;
}