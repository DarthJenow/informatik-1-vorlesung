/*
 * Dateiname:			aufgabe_1.cpp
 * Datum:				2021-05-20 02:22:00
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 02:22:00
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main(int argc, char const *argv[]) {
	srand(clock());

	int wuerfe = 20; //INT32_MAX;
	int seiten = 6;

	int wuerfel[seiten];

	for (int i = 0; i < seiten; i++) {
		wuerfel[i] = 0;
	}

	for (int i = 0; i < wuerfe; i++) {
		wuerfel[rand() % seiten]++;
	}

	for (int i = 0; i < seiten; i++) {
		cout << "Die Ziffer " << i + 1 << " wurde zu " << (double) wuerfel[i] / wuerfe * 100 << "% geworfen" << endl;
	}
	
	cout << "\n\n\n\n";

	/**
	 * ROULETTE
	 */

	char s;
	int z = -1, kugel;
	bool win = 0;

	cout << "Willkommen am Roulettetisch! Sie Sie koenne auf folgendes setzten:" << endl;
	cout << "1) Reihe 1 (1, 4, 7, ..., 34)" << endl;
	cout << "2) Reihe 2 (2, 5, 8, ..., 35)" << endl;
	cout << "3) Reihe 3 (3, 6, 9, ..., 36)" << endl;
	cout << "4) Gerade" << endl;
	cout << "5) Ungerade" << endl;
	cout << "6) Einzelne Zahl" << endl;
	cin >> s;

	if (s >= '1' && s <= '6') {
		if (s == '6') {
			cout << endl << "Auf welche Zahl moechten Sie setzen? (1-36) ";
			
			do {
				cin >> z;

				if (z < 1 || z > 36) {
					cout << "Ungueltige Zahl. Bitte waehlen Sie eine gueltige Zahl (1-36): " << flush;
				}
			} while (z < 1 || z > 36);
		}

		cout << endl << "Die Zahl rollt" << endl;
		cout << "..." << endl;

		kugel = rand() % 37;

		if (kugel != 0) {
			switch (s) {
				case '1':
					win = kugel % 3 == 1;
					break;
				case '2':
					win = kugel % 3 == 2;
					break;
				case '3':
					win = kugel % 3 == 0;
					break;
				case '4':
					win = kugel % 2 == 0;
					break;
				case '5':
					win = kugel % 2 == 1;
					break;
				case '6':
					win = kugel == z;
					break;
			}
		}
		

		cout << endl << "Die Kugel ist auf " << kugel << " liegen geblieben." << endl;

		if (win) {
			cout << "Sie haben gewonnen!" << endl;
		} else {
			cout << "Sie haben verloren!" << endl;
		}
	} else {
		cout << "Ungueltige Eingabe!" << endl;
	}

	return 0;
}