/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				2021-05-20 03:07:33
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 03:07:33
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>
using namespace std;

int main(int argc, char const *argv[]) {
	for (char i = 'A'; i <= 'Z'; i++) {
		cout << (char) (i + pow(-1, i) * 16 + 16);
	}

	cout << endl;
	
	return 0;
}