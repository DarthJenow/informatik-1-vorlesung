/*
 * Dateiname:			aufgabe_4.cpp
 * Datum:				2021-05-20 03:39:19
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 03:39:19
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	string input_string;

	int anzahl_nspace, anzahl_e = 0;
	int first_space;
	int last_space;

	cout << "Geben sie eien String ein: ";
	getline(cin, input_string);
	
	anzahl_nspace = input_string.length();

	for (int i = 0; i < input_string.length(); i++) {
		// Leerstellen zählen
		if (input_string.at(i) == ' ') {
			anzahl_nspace--;
		} else if (input_string.at(i) == 'e' || input_string.at(i) == 'E') {
			anzahl_e++;

			input_string = input_string.erase(i, 1);

			i--;
		}
	}

	first_space = input_string.find(' ');
	last_space = first_space;

	for (int i = 0; i < input_string.length(); i++) {
		if (input_string.at(i) == ' ') {
			last_space = i;
		}
	}

	cout << "Dein Satz enhielt " << anzahl_e << " e / E" << endl;
	cout << "Dein String ohne E / e: " << input_string << endl;


	input_string.replace(last_space + 1, input_string.length() - last_space, "Deznuts");
	input_string.erase(0, first_space + 1);

	cout << "Dein String, aber das erste und letzte Wort modifiziert: " << input_string << endl;
	
	return 0;
}