/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-05-06 02:22:35
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 02:22:35
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

void einer_zahl(int z);
void zehner_zahl(int z);
void hunderter_zahl(int z);

int einer = 0;
int zehner = 0;
int hunderter = 0;

int main(int argc, char const *argv[]) {
	int zahl, l;

	cout << "Willkommen zum Zahlensortierer. Bitte geben Sie eine Zahl zwischen 1 und 999 ein: z = ";
	cin >> zahl;
	
	if (zahl < 1 || zahl > 999) {
		cout << "INPUT ERROR - Geben Sie eine Zahl zwischen 1 und 999 ein!" << endl;
	} else if (zahl < 10) {
		einer_zahl(zahl);
		l = 1;
	} else if (zahl < 100) {
		zehner_zahl(zahl);
		l = 2;
	} else {
		hunderter_zahl(zahl);
		l = 3;
	}

	cout << endl << "Die eingegeben Zahl z = " << zahl << " hat n = " << l << " Ziffern." << endl;
	cout << "Zussamengesetzt ergibt sich folgende Zahl: z = ";

	switch (l) {
		case 3:
			cout << hunderter;
		case 2:
			cout << zehner;
		case 1:
			cout << einer;
	}

	cout << endl;

	return 0;
}

void einer_zahl(int z) {
	einer = z;
}

void zehner_zahl (int z) {
	zehner = z / 10;
	einer = z % 10;
}

void hunderter_zahl(int z) {
	hunderter = z / 100;
	zehner = z % 100 / 10;
	einer = z % 10;
}