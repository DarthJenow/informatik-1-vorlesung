/*
 * Dateiname:			aufgabe_1.cpp
 * Datum:				2021-05-06 01:59:23
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 01:59:23
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

void square_down(int z);
void square_up(int z);

int main(int argc, char const *argv[]) {
	int zahl;

	cout << "Geben Sie eine Zahl ein: ";
	cin >> zahl;

	square_down(zahl);
	square_up(zahl);

	cout << endl;
	
	return 0;
}

void square_down(int z) {
	cout << z * z << " ";

	if (z > 0) {
		square_down(z - 1);
	}
}

void square_up(int z) {
	if (z > 1) {
		square_up(z - 1);
	}

	cout << z * z << " ";
}