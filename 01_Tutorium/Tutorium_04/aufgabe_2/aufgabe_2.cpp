/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				2021-05-06 02:13:37
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 02:13:37
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>

using namespace std;

double pi = 3.1416;

double ftm_rechteck(double b, double h);
double ftm_kreis(double r);

int main(int argc, char const *argv[]) {
	double b, h;
	char mode;

	cout << "Willkommen zum Flaechentraegheitsmomentenrechner (was ein langer name Ü)" << endl;
	cout << "Was moechten Sie berechnen?" << endl;
	cout << "(K) - Flaechentraegheitsmoment fuer einen kreisfoermigen Querschnitt" << endl;
	cout << "(R) - Flaechentraegheitsmoment fuer einen rechteckigen Querschnitt" << endl;

	cin >> mode;

	if (mode == 'K' || mode == 'k') {
		cout << endl << "Geben Sie den Querschnittsradius ein: r = ";
		cin >> b;

		cout << "FTM_K (" << b << ") = " << ftm_kreis(b) << endl;
	} else if (mode == 'R' || mode == 'r') {
		cout << endl << "Geben Sie Breite und Hoehe des Rechteckquerschnitts ein: b, h = ";
		cin >> b >> h;

		cout << "FTM_R (" << b << ", " << h << ") = " << ftm_rechteck(b, h) << endl;
	} else {
		cout << "INPUT ERROR - Waehlen Sie einen gueltigen Modus aus!" << endl;
	}
	
	return 0;
}

double ftm_rechteck(double b, double h) {
	return b * pow(h, 3) / 12;
}

double ftm_kreis(double r) {
	return pi * pow(r, 4) / 4;
}