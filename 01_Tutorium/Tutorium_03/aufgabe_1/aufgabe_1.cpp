#include <iostream>
using namespace std;

bool wasMachIch (bool a) { return !a; }
bool wasMachIch (bool a, bool b) { return a || b; }
double wasMachIch (int x, int y) { return x / (2 * y); }
bool wasMachIch (int x) {
	if (x < 20) {
		return true;
	} else {
		return false;
	}
}

int main(int argc, char const *argv[]) {
	int a (11), b = 44;
	bool x = true, y = wasMachIch(a);

	cout << "1. Ausgabe: " << wasMachIch(b, a) << endl;

	if ((a < b) && (y == true)) {
		cout << "2. Ausgabe: " << wasMachIch(x, y) << endl;
	} else {
		cout << "2. Ausgabe: " << wasMachIch(x) << endl;
	}

	if (!wasMachIch(a) == true) {
		cout << "3. Ausgabe: " << wasMachIch(a) << endl;
	} else {
		cout << "3. Ausgabe: " << wasMachIch(b) << endl;
	}

	cout << "4. Ausgabe: " << wasMachIch(y, wasMachIch(x)) << endl;

	system("PAUSE");
	
	return 0;
}