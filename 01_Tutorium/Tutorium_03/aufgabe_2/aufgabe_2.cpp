/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				2021-04-29 02:12:13
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-29 02:12:13
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

double add (double a, double b);
double subtract (double a, double b);
double multiply (double a, double b);
double divide (double a, double b);

int main(int argc, char const *argv[]) {
	double a, b;
	char op;

	cout << "Willkommen zum einfachen Funktionsrechner!" << endl;
	cout << "Geben Sie eine Rechenoperation ein: ";
	cin >> a >> op >> b;

	cout << "=> ";

	switch (op) {
		case '+':
			cout << add(a, b);
			break;
		case '-':
			cout << subtract(a, b);
			break;
		case '*':
			cout << multiply(a, b);
			break;
		case '/':
			cout << divide(a, b);
			break;
		default:
			cout << "Geben Sie einen gültigen Rechenoperator ein!";
			break;
	}

	cout << endl;
	
	return 0;
}

double add (double a, double b) {
	return a + b;
}

double subtract (double a, double b) {
	return a - b;
}

double multiply (double a, double b) {
	return a * b;
}

double divide (double a, double b) {
	return a / b;
}