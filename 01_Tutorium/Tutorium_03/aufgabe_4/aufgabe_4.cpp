/*
 * Dateiname:			aufgabe_4.cpp
 * Datum:				2021-04-29 02:34:02
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-29 02:34:02
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double degree_to_radiant (double arc);
double vierter_winkel(double a, double b, double c);

int main(int argc, char const *argv[]) {
	double a, b, c, d;

	cout << "Willkommen zum Winkelrechner!" << endl;
	cout << "Geben Sie drei Winkel im Gradmaß ein: ";
	cin >> a >> b >> c;

	if ((a + b + c) < 360 && (a * b * c) != 0) {
		d = vierter_winkel(a, b, c);

		cout << "Der vierte Winkel deines Dreiecks beträgt:" << endl;
		cout << "delta = " << d << "°" << endl;

		cout << endl << "Die Winkelgrößen betragen im Bogenmaß:" << endl;
		cout << "alpha = " << degree_to_radiant(a) << endl;
		cout << "beta = " << degree_to_radiant(b) << endl;
		cout << "gamma = " << degree_to_radiant(c) << endl;
		cout << "delta = " << degree_to_radiant(d) << endl;
	} else if ((a * b * c) == 0) {
		cout << "ERROR - Alle Winkel müssen größer als 0° sein" << endl;
	} else if (a + b + c > 360) {
		cout << "ERROR - Die Summe der ersten 3 Winkel muss kleiner als 360° sein" << endl;
	}
	
	return 0;
}

double degree_to_radiant (double arc) {
	return arc / 180 * M_PI;
}

double vierter_winkel (double a, double b, double c) {
	return 360 - a - b - c;
}