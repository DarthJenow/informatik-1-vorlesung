/*
 * Dateiname:			aufgabe_5.cpp
 * Datum:				2021-04-29 02:49:31
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-29 02:49:31
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double v_pyramide(double a, double h);
double a_pyramide(double a);
double v_zylinder(double r, double h);
double a_zylinder(double r);

int main (int argc, char const *argv[]) {
	double pyramide = v_pyramide(1, 2);
	double zylinder = v_zylinder(1, 2);
	
	return 0;
}

double v_pyramide (double a, double h) {
	return 1 / 3 * a_pyramide(a) * h;
}

double a_pyramide (double a) {
	return a * a;
}

double v_zylinder (double r, double h) {
	return 1 / 3 * a_zylinder(r) * h;
}

double a_zylinder (double r) {
	return r * r;
}