#include <iostream>
#include <cstdlib>
using namespace std;

int wunder (int a, int b, int c) { return a + b + c; }
float wunder (int a) { return a / 2.0; }
float wunder (float x, int a ) { return x * a; }
int wunder (float x) { return x - 1; }

int main(int argc, char const *argv[]) {
	int aa(1), bb(5), cc(3);

	float xx = 2.5, yy = 3.33;

	cout << "1. Wunder: " << wunder(yy) << endl;
	cout << "2. Wunder: " << wunder(bb) << endl;
	cout << "3. Wunder: " << wunder(xx, cc) << endl;
	cout << "4. Wunder: " << wunder(cc, aa, wunder(xx)) << endl;

	system("PAUSE");
	
	return 0;
}