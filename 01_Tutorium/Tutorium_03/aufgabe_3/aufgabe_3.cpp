/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-04-29 02:30:33
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-29 02:30:33
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int telefonkarte = 15;

void telefonieren();

int main(int argc, char const *argv[]) {
	telefonieren();

	for (int i = 0; i < 3; i++) {
		telefonieren();
	}
	
	return 0;
}

void telefonieren() {
	telefonkarte -= 3;

	switch (telefonkarte) {
		case 15:
		case 12:
			cout << "Karte fast voll" << endl;
			break;
		case 9:
		case 6:
			cout << "Karte ca. halb voll" << endl;
			break;
		default:
			cout << "Karte auffüllen" << endl;
	}
}