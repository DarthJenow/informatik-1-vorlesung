/*
 * Dateiname:			aufgabe2.cpp
 * Datum:				2021-04-22 02:08:24
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-22 02:08:24
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	bool a, b, y_a, y_b;

	a = true;
	b = true;

	y_a = (!(a || b) && (a || !b)) || (!a && b);
	y_b = (!(a && b) || (a && !b)) && (!a || b);

	cout << "Aufgabenteil A : y = " << y_a << endl;
	cout << "Aufgabenteil B: y = " << y_b << endl;
	
	return 0;
}