/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-04-22 02:13:37
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-22 02:13:37
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	double a, b;
	char op;

	cout << "Geben Sie eine Rechnung ein: ";
	cin >> a >> op >> b;

	switch (op) {
		case '+':
			a += b;
			break;
		case '-':
			a -= b;
			break;
		case '*':
			a *= b;
			break;
		case '/':
			if (b != 0) {
				a /= b;
			} else {
				op = 'm';
			}
			break;
		default:
			op = 's';
			break;
	}

	switch (op) {
		case 's':
			cout << "SYNTAX ERROR - Geben Sie einen gültigen Rechenoperator ein (+ - * /)" << endl;
			break;
		case 'm':
			cout << "MATH ERROR - Divison durch Null" << endl;
			break;
		default:
			cout << "= " << a << endl;
			break;
	}
	
	return 0;
}