/*
 * Dateiname:			aufgabe_4.cpp
 * Datum:				2021-04-22 02:26:14
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-22 02:26:14
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	char c;

	cout << "ACHTUNG, Zeichenkontrolle! Geben Sie ein Zeichen ein: ";
	cin >> c;

	switch (c) {
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
			cout << "Zahl zwischen 1 - 5";
			break;
		
		case ';':
			cout << "Semikolon";
			break;
		case '.':
			cout << "Punkt";
			break;
		case ',':
			cout << "Komma";
			break;
		case '!':
		case '?':
			cout << "Ausrufe- oder Fragezeichen";
			break;
		default:
			cout << "Zeichen wird nicht ausgewertet";
			break;
	}

	cout << endl;
	
	return 0;
}