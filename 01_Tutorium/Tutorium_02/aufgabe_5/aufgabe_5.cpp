/*
 * Dateiname:			aufgabe_5.cpp
 * Datum:				2021-04-22 02:31:08
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-22 02:31:08
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <bits/stdc++.h> // fügt INT_MIN und INT_MAX hinzu

#include <iostream>
using namespace std;


bool validate_code(int code);

int main(int argc, char const *argv[]) {
	int code_eingabe;
	bool code_result;

	// // Von Nutzer eingegebenen Code validieren
	// cout << "Geben Sie einen gültigen Code ein, um den Tresor zu öffnen! ";
	// cin >> code_eingabe;

	// code_result = validate_code(code_eingabe);

	// if (code_result) {
	// 	cout << "Bitte warten Sie, der Tresor öffnet sich" << endl;
	// } else {
	// 	cout << "Bitt warten Sie einene Moment" << endl;
	// 	cout << "**calling 112**" << endl;
	// }

	// Alle möglichen Integer-Zahlen durchprobieren
	for (int i = INT_MIN; i < INT_MAX; i++) {
		if (validate_code(i)) {
			cout << i << endl;
		}
	}
	
	return 0;
}

bool validate_code(int code) {
	bool res;

	res = code >= 1000 && code < 100000; // code ist vier bis fünfstellig

	res = res && !(code % 2 == 0 || code % 3 == 0 || code % 4 == 0); // code ist nicht Vielfaches 2, 3, 4

	res = res && code % 1115 == 0; // code ist vielfaches von 1115

	return res;
}