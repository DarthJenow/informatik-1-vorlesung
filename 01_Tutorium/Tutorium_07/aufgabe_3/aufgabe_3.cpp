/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-06-07 04:22:55
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-07 04:22:55
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

void matrix_input(int m[10][10], int x, int y);
void matrix_multiply(int a[10][10], int b[10][10], int res[10][10], int ax, int ay, int by);

int main(int argc, char const *argv[]) {
	int ax, ay, bx, by;

	int a[10][10], b[10][10], res[10][10];

	cout << "Geben Sie die Groeße der ersten Matrix ein: ";
	cin >> ax >> ay;

	cout << "Geben Sie die Groeße der zweiten Matrix ein: ";
	cin >> bx >> by;
	
	if (ay == bx) {
		cout << "\n1. Matrix" << endl;
		matrix_input(a, ax, ay);
		cout << "\n2. Matrix" << endl;
		matrix_input(b, bx, by);

		matrix_multiply(a, b, res, ax, ay, by);

		cout << "Beide Matrizen Multipliziert ergibt:" << endl;

		for (int x = 0; x < ax; x++) {
			for (int y = 0; y < by; y++) {
				cout << res[x][y] << '\t';
			}
			cout << endl;
		}
	} else {
		cout << "MATH ERROR - Die Matrizen können nicht multipliziert werden." << endl;
	}

	return 0;
}

void matrix_input(int m[10][10], int x, int y) {
	for (int i = 0; i < x; i++) {
		cout << "Geben Sie die " << i + 1 << ". Zeile ein: ";
		for (int j = 0; j < y; j++) {
			cin >> m[i][j];
		}
	}
}

void matrix_multiply(int a[10][10], int b[10][10], int res[10][10], int ax, int ay, int by) {
	for (int x = 0; x < ax; x++) {
		for (int y = 0; y < by; y++) {
			res[x][y] = 0;

			for (int i = 0; i < ay; i++) {
				res[x][y] += a[x][i] * b[i][y];
			}
		}
	}
}