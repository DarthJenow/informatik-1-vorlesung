/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				2021-06-07 04:04:11
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-07 04:04:11
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <string>
using namespace std;

void ausgabe(char m[6][4]);

int main(int argc, char const *argv[]) {
	char m_char[6][4];
	
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			m_char[i][j] = 'a' + 4 * i + j;
		}
	}

	ausgabe(m_char);

	int m_int[6][4] = {
		7, -1, 13, 12,
		20, 8, -2, 15,
		16, -5, 10, 6,
		-5, -13, 1, 0,
		8, -4, -14, 3,
		4, -17, -22, -6
	};

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			m_char[i][j] += m_int[i][j];
		}
	}

	ausgabe(m_char);

	string s = "";

	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 4; j++) {
			s += m_char[i][j];
		}
	}

	cout << "\nAlle Chars hintereinander in einem String sind: " << s << endl;

	return 0;
}

void ausgabe(char m[6][4]) {
	cout << "---\t\t\t\t\t---" << endl;

	for (int i = 0; i < 6; i++) {
		cout << "|\t";

		for (int j = 0; j < 4; j++) {
			cout << m[i][j] << '\t';
		}

		cout << "   |\n";
	}

	cout << "---\t\t\t\t\t---" << endl;
}