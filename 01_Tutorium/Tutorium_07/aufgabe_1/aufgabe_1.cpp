/*
 * Dateiname:			aufgabe_1.cpp
 * Datum:				2021-06-07 03:54:45
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-07 03:54:45
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main(int argc, char const *argv[]) {
	srand(clock());
	
	int z[10][10];

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			z[i][j] = rand() % 101;

			cout << z[i][j] << '\t';
		}

		cout << endl;
	}

	int x_min = 0, y_min = 0, x_max = 0, y_max = 0;

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (z[i][j] < z[x_min][y_min]) {
				x_min = i;
				y_min = j;
			} else if (z[i][j] > z[x_max][y_max]) {
				x_max = i;
				y_max = j;
			}
		}
	}

	cout << "\nDie kleinste Zahl ist " << z[x_min][y_min] << " an der Stelle [" << x_min << ", " << y_min << "]." << endl;
	cout << "Die groeßte Zahl ist " << z[x_max][y_max] << " an der Stelle [" << x_max << ", " << y_max << "]." << endl;

	return 0;
}