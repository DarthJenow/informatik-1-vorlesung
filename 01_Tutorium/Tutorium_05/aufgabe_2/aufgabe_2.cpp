/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				2021-05-20 02:02:09
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 02:02:09
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	double konto = 0, muenz;
	int schein;

	char s;

	cout << "Willkommen am Bankautomat!" << endl;

	do {
		cout << "Was moechten sie einbezahlen?" << endl;
		cout << "1) Scheine" << endl;
		cout << "2) Meunezn" << endl;
		cout << "#) abbrechen" << endl;
		
		cin >> s;
		
		switch (s){
			case '1':
				cout << endl << "Welchen Betrag möchten Sie in Scheinen einbezahlen? ";
				cin >> schein;

				konto += schein;
				break;
			case '2':
				cout << endl << "Welchen Betrag möchten Sie in Muenzen einbezahlen? ";
				cin >> muenz;

				konto += muenz;
				break;
		}
	} while (s != '#');

	cout << endl << "Einzahlung beendet!" << endl;
	cout << "Ihr Kontostand betraegt: " << konto << " Euro." << endl;
	
	return 0;
}