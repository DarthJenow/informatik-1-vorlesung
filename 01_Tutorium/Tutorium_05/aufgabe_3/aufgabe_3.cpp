/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-05-20 02:10:15
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 02:10:15
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <string>
using namespace std;

int mysecret (int n);

int main(int argc, char const *argv[]) {
	for (int i = 9; i >= 0; i -= 3) {
		cout << "mysecret(" << i << ") = " << mysecret(i) << endl;
	}
	return 0;
}

int mysecret (int n ) {
	if (n == 0) {
		return 0;
	} else if (n == 1) {
		return 2;
	} else {
		return 2 * mysecret(n - 2) + 2;
	}
}

/**
 * AUSGABE
 * 9: 62
 * 6: 14
 * 3: 6
 * 0: 0
 */