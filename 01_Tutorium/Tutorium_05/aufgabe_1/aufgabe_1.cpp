/*
 * Dateiname:			aufgabe_1.cpp
 * Datum:				2021-05-20 10:36:53
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 10:36:53
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Getränkeautomat: Getränke kaufen
 * Eingabe:				Auswahl, Münzen
 * Ausgabe:				Restbetrag
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	int auswahl, menge;
	double restbetrag, munz_einwurf;

	do {
		cout << "Getraenkeautomat\n";
		cout << "\nWaehlen Sie ihr Getraenk aus:\n";
		cout << "1) Wasser (0,50 Euro)\n";
		cout << "2) Limonade (1,00 Euro)\n";
		cout << "3) Bier (2,00 Euro)\n";
		cout << "0) Automat verlassen\n";

		cout << "\nGeben Sie 1, 2, 3 oder 0 ein: ";
		cin >> auswahl;

		if (auswahl >= 1 && auswahl <= 4) {
			cout << "\nGeben Sie die gewuenschte Menge ein: ";
			cin >> menge;

			switch (auswahl) {
				case 1:
					restbetrag = 0.5;
					break;
				case 2:
					restbetrag = 1;
					break;
				case 3:
					restbetrag = 2;
					break;
			}

			restbetrag *= menge;

			cout << "\n--- Bezahlvorgang ---\n";

			while (restbetrag > 0) {
				cout << endl << "Es fehlen noch " << restbetrag << " Euro.\n";
				cout << "Bitte werfen Sie ein Geldstueck ein: ";
				cin >> munz_einwurf;

				restbetrag -= munz_einwurf;
			}

			cout << "\n" << "--- Getraenkeausgabe ---" << "\n\n";

			for (int i = 1; i <= restbetrag; i++) {
				cout << "Flasche " << i << " von " << menge << " wurde ausgegeben." << "\n";
			}

			cout << "\n" << "Vielen Dank, bitte entnehmen Sie ihre Getraenke." << "\n\n\n" << endl;


		} else if (auswahl != 0) {
			cout << "Ungueltige Auswahl! Bitte geben Sie eine 1, 2, 3 oder 0 ein!" << endl;
		}

	} while (auswahl != 0);
	
	cout << "\nSie haben den Automaten verlassen." << endl;

	return 0;
}