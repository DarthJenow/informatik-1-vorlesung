/*
 * Dateiname:			aufgabe_2.cpp
 * Datum:				/aufgabe_2.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-08 02:11:17
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int menge_zahlen = 5;
	int nutzer_zahlen[menge_zahlen];
	int sortierte_zahlen[menge_zahlen];
	int temp;

	for (int i = 0; i < menge_zahlen; i++) {
		cout << "Geben sie die " << i + 1 << ". Zahl ein: ";
		cin >> nutzer_zahlen[i];
	}

	for (int i = 0; i < menge_zahlen ; i++) {
		for (int j = 0; j < menge_zahlen - i - 1; j++) {
			if (nutzer_zahlen[j] > nutzer_zahlen[j + 1]) {
				temp = nutzer_zahlen[j];
				nutzer_zahlen[j] = nutzer_zahlen[j + 1];
				nutzer_zahlen[j + 1] = temp;
			}
		}
	}

	for (int i = 0; i < menge_zahlen; i++) {
		cout << nutzer_zahlen[i] << endl;
	}

	return 0;
}
