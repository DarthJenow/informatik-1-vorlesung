/*
 * Dateiname:			aufgabe_1.cpp
 * Datum:				/aufgabe_1.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-08 02:03:53
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int hoehe = 10, breite = 19;

	for (int i = 0; i < hoehe; i++) {
		for (int j = 0; j < i; j++) {
			cout << " ";
		}
		cout << "*";
		
		for (int j = i; j < breite - i; j++){
			cout << " ";
		}

		cout << "*" << endl;
	}

	for (int i = 0; i < breite / 2 + 1; i++) {
		cout << " ";
	}

	cout << "*" << endl;

	return 0;
}
