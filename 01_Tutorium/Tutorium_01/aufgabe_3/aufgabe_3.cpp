/*
 * Dateiname:			aufgabe_3.cpp
 * Datum:				2021-04-15 03:46:00
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-15 03:46:00
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Ermittelt, wann eine Geldsumme mit festem Zinssatz eine Million erreicht hat
 * Eingabe:				Startkapital, Zinssatz
 * Ausgabe:				Dauer
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	double geld, zinssatz;
	int jahr;

	cout << "Geben Sie ihr Startkapital in Euro und die Verzinsung in Prozent ein: ";
	cin >> geld >> zinssatz;

	while (geld < 1000000) {
		geld *= 1 + zinssatz / 100;

		cout << geld << endl;

		jahr++;
	}

	cout << "Du bist in " << jahr << " Jahren Millionär! Dein Kontostand beträgt " << geld << "€" << endl;
	
	return 0;
}