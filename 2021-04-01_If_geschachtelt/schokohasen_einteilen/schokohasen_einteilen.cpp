/*
 * Dateiname:			schokohasen_einteilen.cpp
 * Datum:				/schokohasen_einteilen.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-01 11:37:57
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				gibt an, ob ein Schokohase sehr süß, süß oder wenig süß ist
 * Eingabe:				Gewicht der Schokohasen, Kakoanteil in %
 * Ausgabe:				Kakaogewicht, Kategorie
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	float gesamtgewicht, kakoanteil, kakaogewicht;

	int sehr_suess_max = 32;
	int suess_max = 65;

	cout << "Willkommen zum Schokohasenkategorisierungsprogramm!" << endl;
	cout << "Wie viel wiegt Ihr Schokohase in Gramm? m = ";
	cin >> gesamtgewicht;

	cout << "Wie hoch ist der Kakaoanteil Ihres Schokohasen in Prozent? n = ";
	cin >> kakoanteil;

	kakaogewicht = gesamtgewicht * kakoanteil / 100;

	cout << "Ihr Schokohase ist ";
	if (kakoanteil < sehr_suess_max) { // --> Werte kakaoanteil < sehr_suess_max
		cout << "sehr suess";
	}
	else if (kakoanteil < suess_max) { // Werte kakaoanteil liegen im Intervall [sehr_suess_max; suess_max]
		cout << "suess";
	}
	else  { // Werte kakoanteil > suess_max
		cout << "wenig suess";
	}
	cout << "!" << endl;

	cout << "Ihr Schokohase enthält m_K = " << kakaogewicht << "g Kakao." << endl;

	return 0;
}
