/*
 * Dateiname:			schaltjahre_finden.cpp
 * Datum:				/schaltjahre_finden.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-01 12:17:51
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Bestimmt, ob eine eingegebene Jahreszahl ein Schaltjahr ist
 * Eingabe:				Jahreszahl
 * Ausgabe:				Kategorie: ist (kein) Schaltjahr
 */

#include <iostream>
using namespace std;

/*
 * REGELN
 * durch 4 teilbar --> Schaltjahr
 * 		Ausnahme: durch 100 teilbar --> kein Schaltjahr
 * 			Ausnahme: durch 400 teilbar --> Schaltjahr
 */

int main(int argc, char const *argv[])
{
	int jahr;

	cout << "Geben Sie ein Jahr ein, das überprüft werden soll: ";
	cin >> jahr;

	if (jahr % 4 == 0)
	{ // 4er Jahr? --> Fallunterscheidung mit 100er / 400er Jahre
		if (jahr % 100 == 0) // 100er Jahr --> möglicherweise kein Schaltjahr
		{
			if (jahr % 400 == 0)
			{ // 400er --> doch Schaltjahr
				cout << jahr << " ist ein Schaltjahr." << endl;
			}
			else // 100er aber kein 400er --> kein Schaltjahr
			{
				cout << jahr << " ist kein Schaltjahr." << endl;
			}
		}
		else // kein 100er --> Schaltjahr
		{
			cout << jahr << " ist ein Schaltjahr." << endl;
		}
	}
	else
	{ // kein 4er Jahr --> kein Schaltjahr
		cout << jahr << " ist kein Schaltjahr." << endl;
	}
	
	return 0;
}
