/*
 * Dateiname:			kreisflaeche.cpp
 * Datum:				2021-04-21 11:10:33
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-21 11:10:33
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Kreisfläche für einen gegebenen Radius
 * Eingabe:				Radius
 * Ausgabe:				Kreisfläche
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double kreis_flaeche(double radius);
/**
 * Deklaration einer Funktion Namens kreis_flaeche mit
 * Rückgabetyp double --> das heißt gibt ein Ergebnis vom Typ double zurück
 * Erwartet einen Übergabewert vom Typ double, hier radius genannt
 */

int main(int argc, char const *argv[]) {
	double radius, flaeche;

	cout << "Geben Sie einen Kreisradius ein: r = ";
	cin >> radius;

	flaeche = kreis_flaeche(radius);

	cout << "Ein Kreis mit dem Radius r = " << radius << " hat die Flaeche A = " << flaeche << endl;
	
	return 0;
}

double kreis_flaeche(double radius) {
	return radius * radius * M_PI;
}