/*
 * Dateiname:			tannenbaum.cpp
 * Datum:				2021-04-21 10:37:11
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-21 10:37:11
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Tannenbaum aus Zeichen schreiben lassen per Funktion
 * Eingabe:				Radius Tannenbaum
 * Ausgabe:				Tannenbaum
 */

#include <iostream>
using namespace std;

void tannenbaum(int radius);
void tannen_schicht(int platz, int zeichen);

int main(int argc, char const *argv[]) {
	int radius;

	cout << "Willkommen zum Tannenbaum-Zeichner! Geben Sie den gewuenschten Radius des Tannenbaums ein: ";
	cin >> radius;
	
	tannenbaum(radius);

	return 0;
}

// erstellt einen kompletten Tannenbaum mit Stamm
void tannenbaum(int radius){
	// Den Tannenbaum schichtenweise aufbauen
	for (int i = 1; i <= radius; i++) {
		tannen_schicht(radius - i, i * 2 - 1);
	}

	// Stamm erstellen
	for (int i = 1; i <= radius / 7; i++) {
		tannen_schicht(radius - 1, 1);
	}
}

// erstellt eine Schicht des Tannenbaums
void tannen_schicht(int platz, int zeichen) {
	for (int i = 0; i < platz; i++) {
		cout << ' ';
	}

	for (int i = 0; i < zeichen; i++) {
		cout << "*";
	}

	cout << endl;
}