/*
 * Dateiname:			dreieck.cpp
 * Datum:				2021-04-21 10:13:20
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-21 10:13:20
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Dreieck aus Zeichen schreiben lassen per Funktion
 * Eingabe:				Zeichen
 * Ausgabe:				Dreieck aus Zeichen
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Schreiben Sie eine Funktion dreieck, die ein Zeichen und die Azahl an Zeilen als
 * Eingabewerte (Argumente) bekommt und dann ein folgendes Dreieck, wie mit z.B. * und 5 ausgibt.
 * *
 * **
 * ***
 * ****
 * *****
 * ******
 * *******
 * 
 * optional: Teile der Funktion dreieck in eine Funktion linie auslagern
 */

void dreieck(char zeichen, int zeilen);
void linie(char zeichen, int laenge);

int main(int argc, char const *argv[]) {
	char zeichen; // speichert das Zeichen
	int zeilen; // speichert die Anzahl der gewünschten Zeilen

	cout << "Willkommen zum Dreieck-Zeichner. Geben sie ein Zeichen und die Anzahl Linien ein: ";
	cin >> zeichen >> zeilen;
	
	// Dreieck erstellen
	dreieck(zeichen, zeilen);

	return 0;
}

// erstellt ein Dreieck mit einem vorgegebenen Zeichen und vorgegebener Anzahl Zeilen
void dreieck(char zeichen, int zeilen) {
	// Zeilen hochzählen
	for (int i = 1; i <= zeilen; i++) {
		linie(zeichen, i);
	}
}

// gibt eine Zeile mit vorgegebener Anzahl eines vorgegebenen Zeichens in einer Zeile aus
void linie(char zeichen, int laenge) {
	// Menge an Zeichen hinzufügen
	for (int i = 1; i <= laenge; i++){
		cout << zeichen;
	}

	// Linie abschließen
	cout << endl;
}