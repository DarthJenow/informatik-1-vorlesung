/*
 * Dateiname:			dynamic-pointers.cpp
 * Datum:				2021-06-11 08:43:47
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-11 08:43:47
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Reservieren / Belegen von zusätzlichem Speicherplatz zur Luafzeit
 * Eingabe:				double
 * Ausgabe:				Speicherzelleninhalte und Adresse
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	double * container = new double;
	/**
	 * new legt zur Laufzeit des Programs (nicht vor Start!)
	 * einen zusätzlichen Speicherplatz an, hier vom Typ double,
	 * das heißt Gröpe 8 Byte, als double zu lesen
	 * 
	 * allgemein: new Typ;
	 * new liefert die ADRESSE des neuen Speicherplatzes,
	 * der neue Speicherplatz hat KEINEN Namen, wie bei einer Variablen
	 */

	*container = 2.718;
	/**
	 * Zuweisen eines Wertes zum neeuen SPeicherplatz
	 * Zugriff über die Adresse, die in der Variablen container gespeichert ist
	 */
	
	cout << "Adresse des neuen Speicherplatzes: " << container << endl;
	cout << "Inhalt des neuen Speicherplatzes: " << *container << endl;

	delete container; 
	/**
	 * Gibt den Speicherplatz wieder frei, so dass dieser dem Computer
	 * wieder zur Verfügung steht
	 */

	return 0;
}