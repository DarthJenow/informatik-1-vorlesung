/*
 * Dateiname:			dynamic-arrays.cpp
 * Datum:				2021-06-11 09:24:44
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-11 09:24:44
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	int groesse;
	
	cout << "Geben sie die Groesse des Arrays an: ";
	cin >> groesse;

	double* meins = new double[groesse];
	/**
	 * Belegen von zusätzlichem Speicher zur Laufzeit
	 * Erzeugen eines neuen, passgenauen Arrays mit:
	 * new Typ[Groesse] --> liefert Adrese des Arrays
	 */

	
	
	return 0;
}