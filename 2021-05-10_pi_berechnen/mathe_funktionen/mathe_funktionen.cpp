/*
 * Dateiname:			mathe_funktionen.cpp
 * Datum:				2021-05-10 10:03:05
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-10 10:03:05
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Länge eines 3D-Vektors
 * Eingabe:				3 Koordinaten
 * Ausgabe:				Länge (Betrag / Norm)
 */

#include <iostream>
#include <cmath> // C++-Mathebibliothek
using namespace std;

double vektor_laenge(double x, double y, double z);

int main(int argc, char const *argv[]) {
	double k_x, k_y, k_z;

	cout << "Geben Sie 3 Koordinaten eines Vektors ein: ";
	cin >> k_x >> k_y >> k_z;

	cout << "Der Betrag des Vektors:" << endl;
	cout <<  "|(" << k_x << ", " << k_y << ", " << k_z << ")| = " << vektor_laenge(k_x, k_y, k_z) << endl;
	
	return 0;
}

double vektor_laenge(double x, double y, double z) {
	return sqrt(pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0));
}