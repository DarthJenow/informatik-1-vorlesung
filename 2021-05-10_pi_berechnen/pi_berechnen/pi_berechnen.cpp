/*
 * Dateiname:			pi_berechnen.cpp
 * Datum:				2021-05-10 09:54:23
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-10 09:54:23
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Nähert Kreiszahl pi iterativ (und rekurisv) an
 * Eingabe:				
 * Ausgabe:				pi auf 9 Stellen hinter dem Komma genau
 */

#include <iostream>
#include <cmath> // C++-Mathebibliothek
#include <iomanip>
#include <math.h>
#define _USE_MATH_DEFINES
using namespace std;

long double calc_pi_iter();
long double calc_pi_rec(int n);

int main(int argc, char const *argv[]) {
	cout << "iteratives pi = " << fixed << setprecision(15) << calc_pi_iter() << endl;
	
	for (int i = 1; i <= 40; i++) {
		cout << "rec_pi(" << i << ") = " << calc_pi_rec(i) << endl;
	}

	return 0;
}

long double calc_pi_iter() {
	/**
	 * TIPPS
	 * iterative: klein --> groß
	 * 1. Näherung ist gegeben
	 * Variablen für Vorgänger und aktuelle Näherung
	 * 
	 * Laufvariablen? Wenn ja, Startwert? Wo erhöhen?
	 * Schleifenart?
	 * Schleifenbedingung --> Betrag (Differenz aus Vorgänguer und aktuellem Wert) > 0.0000000001
	 * 
	 * WICHTIG
	 * bei pow, sqrt: immer long double-werte schreiben, wenn gefordert --> sqrt(2.0)
	 * 
	 * Optional: Kontrollausgabe der aktuellen näherung in jedem Durchlauf
	 */

	long double pi_prev, pi_now, diff = 1;

	pi_now = 2.0 * sqrt(2.0);

	for (long double i = 2.0; diff > 0.0000000001; i++) {
		pi_prev = pi_now;

		pi_now = pow(2.0, i) * sqrt(2.0 - 2.0 * sqrt(1.0 - pow(pi_prev / pow(2.0, i), 2.0)));

		diff = fabs(pi_now - pi_prev);
	}

	return pi_now;
}

long double calc_pi_rec(int n) {
	if (n == 1) {
		return 2.0 * sqrt(2.0);
	} else {
		return pow(2.0, n) * sqrt(2.0 - 2.0 * sqrt(1.0 - pow(calc_pi_rec(n - 1) / pow(2.0, n), 2.0)));
	}
}