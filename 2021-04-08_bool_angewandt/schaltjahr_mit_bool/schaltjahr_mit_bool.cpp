/*
 * Dateiname:			schaltjahr_mit_bool.cpp
 * Datum:				/schaltjahr_mit_bool.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-08 12:38:53
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Ermittelt, ob ein Jahr ein Schaltjahr ist
 * Eingabe:				Jahr
 * Ausgabe:				Wahrheitswert, ob Schaltjahr
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int jahr;

	bool ist_4, ist_100, ist_400;

	cout << "Geben Sie eine Jahreszahl ein: ";
	cin >> jahr;

	ist_4 = jahr % 4 == 0;
	ist_100 = jahr % 100 == 0;
	ist_400 = jahr % 400 == 0;

	/*
	 * Durch 4 teilbar --> Schaltjahr
	 * Durch 100 teilbar --> doch kein Schaltjahr
	 * Durch 400 teilbar --> trotzdem Schaltjahr
	 */
	if (ist_4 && !ist_100 || ist_400) {
		cout << jahr << " ist ein Schaltjahr" << endl;
	} else {
		cout << jahr << " ist kein Schaltjahr" << endl;
	}

	return 0;
}