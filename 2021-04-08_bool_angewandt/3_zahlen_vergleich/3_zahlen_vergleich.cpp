/*
 * Dateiname:			3_zahlen_vergleich.cpp
 * Datum:				/3_zahlen_vergleich.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-08 12:46:08
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Gibt die größte Zahl aus 3 verschienden aus
 * Eingabe:				3 Gleitkommazahlen
 * Ausgabe:				die größte der Zahlen
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	double z1, z2, z3, z_max;

	cout << "Geben sie drei Zahlen ein: ";
	cin >> z1 >> z2 >> z3;

	if (z1 >= z2 && z1 >= z3) {
		z_max = z1;
	} else if (z2 >= z3 && z2 >= z1) {
		z_max = z2;
	} else {
		z_max = z3;
	}

	cout << "Die groeßte Zahl ist " << z_max << endl;

	return 0;
}