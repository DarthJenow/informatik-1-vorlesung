/*
 * Dateiname:			quersumme.cpp
 * Datum:				/quersumme.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-09 08:20:09
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Quersumme bestimmen
 * Eingabe:				ganze Zahlen
 * Ausgabe:				Quersumme als Text
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int zahl_input;

	cout << "Willkommen zum Quersummenprogramm!" << endl;
	cout << "Bitte geben Sie eine ganze Zahl ein: ";
	cin >> zahl_input;

	// unterscheide verschiedene Fälle, abhängig vom Wert der Variablen zahl_input

	switch (zahl_input) {
		/*
		 * zahl_input ist die Variable, die unterschiedliche Werte annehmen kann
		 * verschiedene Werte sollen unterschieden werden
		 * da könnte auch Anstelle einer variablen ein Term / Rechenausdruck stehen
		 * aber kein logischer Term!
		 */
		case 56:
			cout << "Quersumme ist elf" << endl;
			break;
		
		case 50:
		/*
		 * Wenn nach mehreren Werten die selbe Aktionen erfolgen sollen
		 * können diese kombiniert werden durch mehrere cases mit leeren Anweisungen
		 * Dies entspricht einer Verknüfpung mit oder 
		 * Oder selbst kann nicht verwendet werden
		 * Ein case läuft immer durch mit allen Anweisungen bis zum nächsten break
		 */
		case 41:
		case 32:
			cout << "Quersumme ist fuenf" << endl;
			break;

		case 44:
			cout << "Quersumme ist acht" << endl;
			break;

		case 25:
			cout << "Quersumme ist sieben" << endl;
			break;

		default: // else Fall, das heißt, in allen anderen, nicht oben aufgeführten Fällen
			cout << "Quersumme noch nicht hinterlegt, bitte nachprogrammeiren" << endl;
	}		

	return 0;
}