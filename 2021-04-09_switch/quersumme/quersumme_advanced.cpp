/*
 * Dateiname:			quersumme_advanced.cpp
 * Datum:				/quersumme_advanced.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-09 08:39:50
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Quersummen berechnen
 * Eingabe:				ganze Zahlen
 * Ausgabe:				Quersumme
 */

#include <iostream>
using namespace std;

int quersumme_berechnen (int zahl) {
	int quersumme = 0;
	string zahl_string = to_string(zahl);

	for (int i = 0; i < zahl_string.length(); i++) {
		quersumme += stoi(string(1, zahl_string[i]));
	}

	if (to_string(quersumme).length() > 1){
		quersumme = quersumme_berechnen (quersumme);
	}

	return quersumme;
}

int main(int argc, char const *argv[])
{
	string zahl_input;
	int quersumme = 0;

	string quersumme_string[] = {
		"eins", "zwei", "drei", "vier", "fuenf", "sechs", "sieben", "acht", "neun"
	};

	cout << "Willkommen zum Quersummenprogramm!" << endl;
	cout << "Bitte geben Sie eine ganze Zahl ein: ";
	cin >> zahl_input;

	quersumme = quersumme_berechnen(stoi(zahl_input));

	cout << "Quersumme ist " << quersumme_string[quersumme - 1] << endl;

	return 0;
}