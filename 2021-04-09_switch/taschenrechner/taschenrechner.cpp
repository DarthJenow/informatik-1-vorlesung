/*
 * Dateiname:			taschenrechner.cpp
 * Datum:				/taschenrechner.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-09 08:51:27
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/*
 * AUFGABE
 * Programmieren Sie einen einfanchen Taschenrechner, der
 * - vom Benutzer eine intuitiv gestellte Rechnung eineliest in der Form 2 + 3
 * - die richtige Rechenoperation ausführt
 * - das Ergebnis in der Form 2 + 3 = 5 ausgibt
 * - Falscheingaben abfängt
 * - Division durch 0 als Fehelr ausgibt
 */

int main(int argc, char const *argv[])
{
	double z1, z2, res;
	char oper;

	cout << "Geben Sie eine Rechnung ein: ";
	cin >> z1 >> oper >> z2;

	switch (oper)
	{
	case '+':
		res = z1 + z2;
		break;

	case '-':
		res = z1 - z2;
		break;
	
	case '*':
		res = z1 * z2;
		break;
	case '/':
		if (z2 != 0) {
			res = z1 / z2;
		} else {
			cout << "MATH ERROR - Divison durch Null" << endl;

			oper = 'f';
		}

		break;
	
	default:
		cout << "Bitte geben Sie ein gültiges Rechenzeichen ein ( + - * / )" << endl;

		oper = 'f';

		break;
	}

	if (oper != 'f') {
		cout << z1 << " " << oper << " " << z2 << " = " << res << endl;
	}

	return 0;
}
