/*
 * Dateiname:			teilbarkeit.cpp
 * Datum:				/teilbarkeit.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-09 08:34:41
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	/*
	 * AUFGABE1
	 * Prüfen Sie, ob eine eingegebenen Zahl durch 9 teilbar ist und zwar folgendermaßen
	 * Bei Rest 0 geben Sie aus BINGO
	 * Bei rest 1, 2 oder 3 geben Sie aus "knapp drueber"
	 * Bei Rest 4, 5 geben Sie aus "Ungefaehr die Haelfte"
	 * Bei 6, 7, 8 geben sie aus "fast voll"
	 */

	int zahl_input;

	cout << "Geben Sie eine ganze Zahl ein, deren Teilbarkeit durch 9 ueberprueft werden soll: ";
	cin >> zahl_input;

	switch (zahl_input % 9) {
		case 0:
			cout << "BINGO" << endl;
			break;

		case 1:
		case 2:
		case 3:
			cout << "knapp drueber" << endl;
			break;

		case 4:
		case 5:
			cout << "Ungefaehr die Haelfte" << endl;
			break;
			
		default:
			cout << "fast voll" << endl;
	}

	return 0;
}