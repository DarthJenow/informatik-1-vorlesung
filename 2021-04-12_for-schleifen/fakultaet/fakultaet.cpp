/*
 * Dateiname:			fakultaet.cpp
 * Datum:				/fakultaet.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-12 08:54:36
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet die Fakultät einer gegebenen Zahl
 * Eingabe:				ganze Zahl
 * Ausgabe:				Fakultät
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int z, fak = 1;

	cout << "Von welcher Zahl soll die Fakultaet berechnet werden? ";
	cin >> z;
	
	if (z >= 0) {
		for (int i = 1; i <= z; i++) {
			fak *= i;

			cout << i << ". Zwischen-Fakultaet: " << fak << endl;
			cout << "Die Fakultaet " << z << "! = " << fak << endl;
		}

	} else {
		cout << "Bitte geben sie eine positive, ganze Zahl ein!" << endl;
	}

	return 0;
}
