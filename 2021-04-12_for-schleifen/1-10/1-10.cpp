/*
 * Dateiname:			1-10.cpp
 * Datum:				/1-10.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-12 08:19:39
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				for-Schleifen kennenlernen
 * Eingabe:				-
 * Ausgabe:				Zahlenfolgen und Summen
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int von = 1, bis = 10;

	cout << "Ausgabe der Zahlen von " << von << " bis " << bis << "." << endl;

	for (int i = von; i <= bis; i++) {
		cout << i << endl;
	}
	/*
	 * Zählschleife mit Laufvariable i und Startwert "von"
	 * allg.:
	 * for (Definiton und Initilasierung Laufvraible; Schleifenbedingung; Verändern der Laufvariablen) {
	 * 		Anweisungen
	 * }
	 */

	/*
	 * Schleifenbedingungen, das heißt --> Schleife wird nur ausgeführt SO LANGE Bedingung wahr
	 * hier: Schleife läuft solange i <= "bis"
	 * Verändern der Laufvariablen, hier: i erhöhen um 1
	 * i++ steht für i = i + 1, implette Anweisung
	 * Verändern kann auch mit anderen Schrittweiten sein oder rückwärts, das heißt Erniedrigen
	 */

	return 0;
}
