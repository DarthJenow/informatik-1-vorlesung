/*
 * Dateiname:			gerade_summe.cpp
 * Datum:				/gerade_summe.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-12 08:48:10
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Summen aller (geraden) Zahlen in einem definierten Abschnitt
 * Eingabe:				-
 * Ausgabe:				Summe aller Zahlen
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int von = 1, bis = 10;
	
	cout << endl << "Schrittweise aufsummieren" << endl;
	cout << "Ausgabe aller Teilsummen der Zahlen " << von << " - " << bis << endl;

	int summe = 0;

	for (int i = von; i <= bis; i++) {
		summe += i;
		/*
		 * Schrittweises aufsummieren, das heißt in jedem schritt kommt ein Summand
		 * hinzu, hier der aktuelle Wert von i
		 */

		cout << i << ". Teilsumme: " << summe << endl;
	}

	/*
	 * AUFGABE
	 * Bilden Sie die Summe der Zahlen "bis" bis "von"!, das heißt rückwärts
	 * und geben Sie alle Teilsummen aus!
	 */

	cout << endl << "Nochmal Schrittweises aufsummieren, diesmal aber rückwärts" << endl;

	int r_summe = 0;

	for (int i = bis; i >= von; i--) {
		r_summe += i;
		/*
		 * Schrittweises aufsummieren, das heißt in jedem schritt kommt ein Summand
		 * hinzu, hier der aktuelle Wert von i
		 */

		cout << i << ". Teilsumme: " << r_summe << endl;
	}
	
	bis = 100;
	int g_summe = 0;

	cout << endl << "Schrittweisen aufsummieren aller geraden Zahlen: " << von << " - " << bis << "." << endl;

	von = von + von % 2;

	for (int i = von; i <= bis; i += 2) {
		g_summe += i;
	}

	cout << "Die Summe aller geraden Zahl: " << von << " - " << bis << " ist: " << g_summe << endl;
	
	return 0;
}
