/*
 * Dateiname:			vielfache.cpp
 * Datum:				/vielfache.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-12 09:22:19
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Vielfache einer Zahl ausgeben
 * Eingabe:				Gleitkommazahl
 * Ausgabe:				die ersten n-Vielfachen einer Zahl
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	// AUFGABE für Schnelle
    // Geben Sie eine Zahl (kann auch Kommazahl sein) ein sowie die Anzahl der Vielfachen dieser Zahl
    // Geben Sie dann die Reihe aller Vielfachen bis zur gegebenen Anzahl aus
    // Bsp Zahl: 4, Vielfache 6
    // Ausgabe
    /*
    4
    8
    12
    16
    20
    24
    */

	/*
	 * AUFGABE für schnelle
	 * Geben Sie eien Zahl (kann auch eine Kommazahl sein) ein sowie die Anzahl der Vielfachen dieser zahl
	 * Geben Sie dann die Reihe aller Vielfachen bis zur gegebenn Anzahl aus
	 * Bsp. Zahl: 4, Vielfache: 6
	 * Ausgabe:
	 * 4
	 * 8
	 * 12
	 * 16
	 * 20
	 * 24
	 */
	
	double z;
	int n;

	cout << "Geben Sie eien Zahl ein, und die Anzahl der gewünschten Vielfachen" << endl;

	cin >> z >> n;

	for (int i = 1; i <= n; i++) {
		cout << i << ". vielfaches von " << z << " = " << i * z << endl;
	}

	return 0;
}
