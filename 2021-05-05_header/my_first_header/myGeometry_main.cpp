/*
 * Dateiname:			myGeometry_main.cpp
 * Datum:				2021-05-05 11:29:55
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-05 11:29:55
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>

#include "myGeometry.h"

using namespace std;

int main(int argc, char const *argv[]) {
	cout << kreisflaeche(1) << endl;
	
	return 0;
}