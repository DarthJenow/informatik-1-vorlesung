/*
 * Dateiname:			myGeometry.h
 * Datum:				2021-05-05 10:02:03
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-05 10:02:03
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Deklaration von Geometriefunktionen
 */

#pragma once // Verhindert Mehrfach- / Überdefinition von Klassen und Funktionen
#include <math.h>
#define _USE_MATH_DEFINES
/**
 * Bibliotheken, die in einer Headerdatei eingebunden werden, müssen später
 * nicht nochmals eingebunden werden
 */

// Funktionsdeklaration

double kreisflaeche(double radius);
double kreisumpfang(double radius);
double zylindervolumen(double radius, double hoehe);
double zylindermantel(double radius, double hoehe);
double zylinderoberflaeche(double radius, double hoehe); // inklusive Boden und Deckel

// Funktionsdefeinitonen können jetzt hier kommen oder, BESSER, in getrennter cpp-Datei