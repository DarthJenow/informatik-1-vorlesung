/*
 * Dateiname:			myGeometry.cpp
 * Datum:				2021-05-05 10:17:08
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-05 10:17:08
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Implementiert FUnktionen aus der Headerdatei MyGeometry.h
 */

#include "myGeometry.h" // Einbinden der dazugehörigen Headerdatei


double kreisflaeche(double radius);
double kreisumpfang(double radius);
double zylindervolumen(double radius, double hoehe);
double zylindermantel(double radius, double hoehe);
double zylinderoberflaeche(double radius, double hoehe);

double kreisflaeche(double radius) {
	return radius * radius * M_PI;
}

double kreisumpfang(double radius) {
	return 2 * M_PI * radius;
}

double zylindervolumen(double radius, double hoehe) {
	return radius * radius * M_PI * hoehe;
}

double zylindermantel(double radius, double hoehe) {
	return kreisumpfang(radius) * hoehe;
}

double zylinderoberflaeche(double radius, double hoehe) {
	return zylindermantel(radius, hoehe) + 2 * kreisumpfang(radius);
}