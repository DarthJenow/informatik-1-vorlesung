/*
 * Dateiname:			zwei_zahlen_bedingungen.cpp
 * Datum:				/zwei_zahlen_bedingungen.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-29 08:03:34
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:	
 * Eingabe:	
 * Ausgabe:	
 */

/*
 * AUFGABE
 * Schreiben Sie ein Programm, das vom Benutzer zwei ganze Zahlen
 * einliest und berechnet, wie oft die zweite Zahl in die erste passt undd was übrig bleibt
 * geben Sie die Ergebnisse aus
 * Wiederholen Sie die Rechnung und Ausgabe, wenn die erste Zahl
 * - um eins erhöht wird
 * - mit 3 multipliziert wird (die eingegebene erste Zahl!)
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int nutzer_zahl_1, nutzer_zahl_2;

	cout << "Geben Sie zwei ganze Zahlen ein: ";
	cin >> nutzer_zahl_1 >> nutzer_zahl_2;


	int wie_oft = nutzer_zahl_1 / nutzer_zahl_2;
	int rest = nutzer_zahl_1 % nutzer_zahl_2;
	
	cout << "Die erste Zahl passt " << wie_oft << " mal in die zweite Zahl, dabei bleibt ein Rest von " << rest << endl;
	cout << "a / b = " << nutzer_zahl_1 << " / " << nutzer_zahl_2 << " = " << wie_oft << " Rest: " << rest << endl;
	
	nutzer_zahl_1++; // erst Zahl um 1 erhöhen
	
	wie_oft = nutzer_zahl_1 / nutzer_zahl_2;
	rest = nutzer_zahl_1 % nutzer_zahl_2;
	
	cout << endl;
	cout << "Die erste Zahl wurde um 1 erhöht: a = " << nutzer_zahl_1 << endl;
	cout << "Die erste Zahl passt " << wie_oft << " mal in die zweite Zahl, dabei bleibt ein Rest von " << rest << endl;
	cout << "a / b = " << nutzer_zahl_1 << " / " << nutzer_zahl_2 << " = " << wie_oft << " Rest: " << rest << endl;
	
	nutzer_zahl_1--; // erste Zahl wieder auf Ursprung zurück erniedrigen
	nutzer_zahl_1 *= 3; // erste Zahl mit 3 multiplizieren

	wie_oft = nutzer_zahl_1 / nutzer_zahl_2; //  zahlen teilen
	rest = nutzer_zahl_1 % nutzer_zahl_2; // Rest ermitteln
	
	cout << endl;
	cout << "Die erste Zahl wurde mit 3 multiplizeirt: a = " << nutzer_zahl_1 << endl;
	cout << "Die erste Zahl passt " << wie_oft << " mal in die zweite Zahl, dabei bleibt ein Rest von " << rest << endl;
	cout << "a / b = " << nutzer_zahl_1 << " / " << nutzer_zahl_2 << " = " << wie_oft << " Rest: " << rest << endl;

	return 0;
}