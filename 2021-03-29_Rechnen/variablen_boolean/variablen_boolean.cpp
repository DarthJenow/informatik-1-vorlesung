/*
 * Dateiname:			variablen_boolean.cpp
 * Datum:				/variablen_boolean.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-29 08:56:33
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert den Gebrauch von boolschen Variablen
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	 bool b;
	 /*
	  * Definition einer Variablen namens b vom Typ bool
	  * b kann die Werte true (wahr) odder false (falsch) annehmen
	  */

	 b = true; // b bekommt den Wahrheitswert true zugewiesen

	 cout << "Variable b hat den Wahrheitswert " << b << endl;

	cout.setf(cout.boolalpha); // ab sofort wird true / false anstatt von 1 / 0 geschrieben

	 cout << "Variable b schoener ausgegeben " << b << endl;

	 b = 0; // false wird zugewiesen

	 cout << "Variable b hat jetzt den Wahrheitswert " << b << endl;
	 /*
	  * Bei der Zuweisung von Wahrheitswerten kann sowohl ture / false
	  * als auch 1 / 0 verwendet werden
	  */

	// Eingabe von Wahrheitswerten
	cout << endl << "Geben sie einen Wahrheitswert ein: ";
	cin >> b;
	cout << "sie haben folgende Wahrheitswerte eingegeben: " << b << endl;
	// bei der Eingabe von Wahrheitswerten 1 oder 0 schreiben, keine Worte!

	cin.setf(cin.boolalpha);

	b = 0;

	cout << "Geben sie einen Wahrheitswert als Wort ein: ";
	cin >> b;
	cout << "Ihr Wahrheitswert: " << b << endl;


	bool c;

	c = (7 > 9);
	/*
	 * Die Behauptung, 7 sei größer als 9, wird ausgewertet und
	 * der Wahrheitswert davon der boolschen Variablen c zugewiesen
	 */
	cout << endl << "7 > 9 => " << c << endl;

	/*
	 * AUFGABE
	 * Lesen Sie vom Benutzer eine ganze Zahl ein
	 * Stellen Sie die Behauptung auf, dass diese Zahl <= 355 sei
	 * Weisen Sie das Ergebnis der Variablen c zu
	 * Geben Sie einen Aussagesatz dazu aus
	 */

	int n;

	cout << endl << "Geben Sie eine ganze Zahl ein: n = ";

	cin >> n;

	c = 0;
	c = n <= 355;

	cout << "n <= 355" << endl;
	cout << n << " <= 355" << endl;
	cout << "--> " << c << endl;

	/*
	 * AUFGABE2
	 * Lesen Sie vom Benutzer ein Zeichen ein
	 * Prüfen Sie, ob das Zeichen ein # ist
	 * Geben Sie dazu einen Aussagesatz aus
	 */

	char nutzer_zeichen;

	cout << "Geben Sie ein Zeichen ein: ";
	cin >> nutzer_zeichen;

	bool zeichen_vergleich = nutzer_zeichen == '#';

	cout << "Überprüfe: '#' == " << nutzer_zeichen << endl;
	cout << "--> " << zeichen_vergleich << endl;

	return 0;
 }