/*
 * Dateiname:			variablen_char.cpp
 * Datum:				/variablen_char.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-29 08:33:04
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert Gebrauch von Variablen vom Typ char
 * Eingabe:				Zeichen
 * Ausgabe:				Zeichen, bzw. Variablenwerte
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	char zeichen; // Definition einer Variablen vom Typ char, Namens zeichen

	// Eingabe von char
	cout << "Bitte geben sie ein Zeichen ein: ";
	cin >> zeichen;

	cout << "Sie haben \"" << zeichen << "\" eingegeben" << endl;

	// der Variablen ein festes Zeichen zuweisen

	zeichen = 'A'; // Festes Zeichen immer in ' ', sonst wird es nicht als Zeichen erkannt

	cout << "Variable hat jetzt den Wert " << zeichen << endl;

	/* 
	 * Was passiert bei
	 * zeichen = A; // hier erwartet der Computer eine vordefinierte Variable A
	 * --> "error: ‘A’ was not declared in this scope"
	 */ 
	
	// Weisen Sie der Variablen zeichen das Zeichen 7 zu und geben Sie es aus
	zeichen = '7';

	cout << "Variable hat jetzt den Wert " << zeichen << endl;

	/*
	 * Vorsicht, soll eine Ziffer ausgegeben werden, muss diese auchin ' ' stehen, 
	 * sonst wird der Varaible zeichen ein ASCII-Code zugewiesen
	 * 7 steht für den Befehl BELL, der dann bei der Ausgabe ausgeführt wird
	 */

	return 0;
}