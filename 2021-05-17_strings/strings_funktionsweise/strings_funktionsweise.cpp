/*
 * Dateiname:			strings_funktionsweise.cpp
 * Datum:				2021-05-17 11:33:36
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-17 11:33:36
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert strings und string-Funktion
 * Eingabe:				strings
 * Ausgabe:				strings, Funktionswerte
 */

#include <iostream>
#include <string>
/**
 * Einfügen der Bibliothek für Zeichenketten (Strings)
 * string ist eine Klasse, das heißt Bauplan mit Eigenschaften der
 * Zeichenketten (z.B. deren Länge) und Funktionen dazu)
 */
using namespace std;

int main(int argc, char const *argv[]) {
	string vorname, nachname; // Definition zweier Variablen vom Typ string

	// einlesen von strings
	cout << "Vorname: ";
	// cin >> vorname; // Eingelesen wir dnur bis zum Leerzeichen, mehrere worte sind so nicht möglich
	getline(cin, vorname); // Nimmt vom Eingabestrom cin eine ganze Teile und speichert sie in vorname

	cout << "Nachname: ";
	cin >> nachname;

	// Strings ausgeben
	cout << vorname << " " << nachname << endl;
	// Ausgabe von Stringvariable, Stringkonstante (Text in " "), weiterer Stringvariablen

	// Aneinanderhängen von String (Konkatenation)
	string name = vorname + " " + nachname;
	// hängt Stringvariablen und Stringkonstanten aneinander

	cout << "Guten Morgen " << name << endl;

	// Strings vergleichen
	if (name == "Catheria Burghart") {
		cout << "Guten Morgen, Frau Professor!" << endl;
	} else {
		cout << "Willkommen zur Vorlesung!" << endl;
	}
	/**
	 * VORSICHT
	 * Hier ist absolute Gleichheit gemeint, auch bezüglich Groß-/Kleinschreibung und
	 * Anzahl der Leerzeichen
	 */

	// Sortierung von Strings
	if (vorname < nachname) {
		cout << vorname << " kommt vor " << nachname << endl;
	} else {
		cout << nachname << " kommt vor " << vorname << "oder ist gleich" << endl;
	}
	/**
	 * Folg der Sortierung vom ASCII-Code
	 * Vergleicht bis zum ersten unterschiedlichen Zeichen
	 */
	
	return 0;
}