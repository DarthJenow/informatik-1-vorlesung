/*
 * filename:		variablen.cpp
 * path:			/variablen.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-24 10:02:19
 * date modified:	2021-03-24 10:03:51
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int meine_zahl;

	/*
	 * Deklaration der Variablen meine_zahl vom Typ int (ganze Zahl)
	 * --> 4 Byte Größe im Arbeitsspeicher
	 * --> Lies Bitmuster als ganze Zahl
	 * 
	 * allgemein: "Typ Variablenname"
	 */

	/*
	 * Was steht in der Variablen nach erstellen?
	 * --> zufälliger Zahlenwert!!!
	 */

	/*
	 * Wert zuweisen
	 */

	meine_zahl = 7; 
	/*
	 * Wert 7 wird er Variablen meine_Zahl zugewiesen --> im Arbeitsspeicher gespeichert
	 * links von "=" steht die Variable, die neuen Wert bekommen soll
	 * rechts von "=" steht der Wert (z.B. Zahl oder eine andere Variable) doer Rechenausdruck, der zugewiesen werden soll
	 * "=" heißt Zuweisung
	 */

	// Ausgabe der Variablen
	cout << "Meine Zahl ist " << meine_zahl << endl;

	// einfach Variablennamen in cout angeben

	int neue_zahl;

	// Vom Benutzer Wert einlesen

	cout << "Geben Sie eine ganze Zahl ein: ";
	cin >> neue_zahl;
	/*
	 * Einlesen eines Wertes vom Benutzer --> wir dvom Standardeingabestrom
	 * genommen und in Varialbe neue_zahl gespeichert
	 * mit >> können innerhalb von cin mehrere Eingaben aneinander gehängt werden
	 */
	
	cout << "Ihre Zahl lautet: " << neue_zahl << endl;

	/*
	 * AUFGABE
	 * Definieren sie zwei neue variablen a,b vom Typ int
	 * Lesen Sie beide Variablenwerte vom Benutzer ein, kommunizieren Sie dabei mit ihm
	 * Geben Sie dei Variablenwerte aus
	 */

	return 0;
}