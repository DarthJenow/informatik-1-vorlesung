/*
 * filename:		zwei_variablen.cpp
 * path:			/zwei_variablen.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-24 10:55:00
 * date modified:	2021-03-24 10:55:06
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int a, b;

	cout << "Geben Sie zwei ganze Zahlen ein: ";
	cin >> a >> b;

	cout << "Die Zahlen sind: " << a << " " << b << endl;
	
	/*
	 * Tausche Werte von a und b
	 * 
	 * Geben Sie die Variablen nochmal mit den getauschten Werten aus
	 */

	a = a + b;
	b = a - b;
	a = a - b;

	cout << "Die Zahlen sind: " << a << " " << b << endl;

	return 0;
}