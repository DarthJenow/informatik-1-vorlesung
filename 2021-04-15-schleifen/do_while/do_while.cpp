/*
 * Dateiname:			do_while.cpp
 * Datum:				/do_while.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-15 11:55:22
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Summe Zahlen 1 bis 10, danach: Aufsummieren aller Zahle bis 0 eingegeben wurde
 * Eingabe:				-, zahlen
 * Ausgabe:				Teilsummen
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int von = 1;
	int bis = 10;

	int summe = 0;

	int k = von;

	do {
		summe += k;

		cout << k << ". Teilsumme: " << summe << endl;

		k++;
	} while (k <= bis);


	cout << endl << "Summe aller eingegebnen Zahlen, Ende mit 0" << endl;

	summe = 0;
	int zahl;

	do {
		cout << "Geben Sie eine ganze Zahl ein als naechsten Summanden, Ende mit 0: ";
		cin >> zahl;

		summe += zahl;

		if (zahl != 0) {
			cout << "neue Teilsumme: " << summe << endl;
		}
	} while (zahl != 0);
	
	return 0;
}
