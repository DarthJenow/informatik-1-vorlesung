/*
 * Dateiname:			schleifen_taschenrechner.cpp
 * Datum:				/schleifen_taschenrechner.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-15 12:19:20
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/*
 * AUFGABE
 * Veränder Sie Ihr Taschenrechnerprogramm so,
 * dass der Benutzer immer wieder eine neue Rechnung eingeben kann, solange er nicht
 * als Rechenoperator ein # eingibt
 * Alternative: Der Benutzer wird nach jedem Durchlauf gefragt, und kann auswählen,
 * ob eine neue Rechnung eingegeben werden soll
 */

int main(int argc, char const *argv[])
{
	double z1, z2, res;
	char op;

	do {
		cout << "Geben Sie eine Rechnung ein: ";
		cin >> z1 >> op >> z2;

		switch (op) {
		case '+':
			res = z1 + z2;
			break;
		case '-':
			res = z1 - z2;
			break;
		case '*':
			res = z1 * z2;
			break;
		case '/':
			if (z2 != 0) {
				res = z1 / z2;
			} else {
				cout << "MATH ERROR - Divison durch Null" << endl;

				op = 'f';
			}

			res = z1 / z2;

			break;
		case '#':
			cout << "# erkannt, Programm wird beendet" << endl;
			break;
		default:
			cout << "Bitte geben Sie ein gültiges Rechenzeichen ein ( '+', '-', '*', '7', '#' zum abbrechen" << endl;
			
			break;
		}

		if (op != 'f' && op != '#') {
			cout << z1 << " " << op << " " << z2 << " = " << res << endl;
		}

	} while (op != '#');
	
	return 0;
}
