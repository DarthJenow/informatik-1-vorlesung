/*
 * filename:		variablen.cpp
 * path:			/variablen.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-22 09:01:45
 * date modified:	2021-03-22 05:43:35
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	/*
	 * Definition einer Variablen namens meineZahl vom Typ int, d.h.
	 * es werden im arbeitsspeicher 4 zusammenhängende Byte (24 Bit) belegt und
	 * als ganze Zahl (int) interpretiert
	 * Die Variable ist über den Namen ansprechbar
	 * 
	 * Definition allgemein:
	 * Typ Variablennamen;
	 */
	int meineZahl;

	cout << meineZahl << endl;
	
	return 0;
}