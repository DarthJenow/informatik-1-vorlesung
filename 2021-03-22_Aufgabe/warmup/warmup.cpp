/*
 * filename:		warmup.cpp
 * path:			/warmup.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-22 08:26:51
 * date modified:	2021-03-22 05:43:16
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>
using namespace std;
// Beabsichtigte Methode in der Vorlesung
int main(int argc, char *argv[]) {
	cout << 1 << endl;
	cout << 22 << endl;
	cout << 333 << endl;
	cout << 4444 << endl;
	
	return 0;
}

// // Sinnvollere Methode mit for-loops
// int main(int argc, char *argv[]) {
// 	int iterationen = 4;

// 	for (int i = 1; i <= iterationen; i++) {
// 		for (int j = 0; j < i; j++) {
// 			cout << i;
// 		}

// 		cout << endl;
// 	}
	
// 	return 0;
// }