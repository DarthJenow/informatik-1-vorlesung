/*
 * Dateiname:			while_schleife.cpp
 * Datum:				/while_schleife.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-14 11:01:45
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int summe = 0;
	int w = 1; // Laufvariable mit Anfangswert 1

	/*
	 * while-Schleife prüft Bedingung zu Beginn ab , bevor sie jedesmal
	 * betreten wird
	 */

	while (w <= 10) {
		summe += w;

		cout << w << ". Teilsumme " << summe << endl;

		w++;
	}

	// Anderes Beispiel 

	cout << endl << "Summe aller eingegebnen Zahlen, Ende mit 0" << endl;
	
	int ergebnis_summe = 0;
	int zahl;

	cout << "Geben Sie eine ganze Zahl ein als naechsten Summanden, Ende mit 0: ";
	cin >> zahl;

	while (zahl != 0) {
		ergebnis_summe += zahl;
		cout << "neue Teilsumme: " << ergebnis_summe << endl;
		
		cout << "Geben Sie eine ganze Zahl ein als naechsten Summanden, Ende mit 0: ";
		cin >> zahl;
	}

	return 0;
}
