/*
 * Dateiname:			potenzen.cpp
 * Datum:				/potenzen.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-14 10:23:02
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet BAsis hoch Exponent
 * Eingabe:				BAsis (Gleitpunkt), Exponent (int)
 * Ausgabe:				Potenz
 */

#include <iostream>
using namespace std;

/*
 * AUFGABE
 * Lseen Sie vom Benutzer Basis und Exponent ein
 * Berechnen Sie di ePotenz mittels for-Schleife
 * Auch negative Exponenten sollen möglich sein
 * Geben Sie das Ergebnis aus!
 */

int main(int argc, char const *argv[])
{
	double basis, ergebnis = 1;
	int potenz;

	cout << "Willkommen zum Exponentenrechner" << endl;
	cout << "Geben Sie eine Basis und einen Exponenten ein: ";
	cin >> basis >> potenz;

	if (potenz < 0) {
		for (int i = 1; i <= - potenz; i++) {
			ergebnis *= basis;
		}

		ergebnis = 1.0 / ergebnis;
	} else {
		for (int i = 1; i <= potenz; i++) {
			ergebnis *= basis;
		}
	}

	

	cout << basis << "^" << potenz << " = " << ergebnis << endl;

	cout << potenz % 1 << endl;

	return 0;
}

