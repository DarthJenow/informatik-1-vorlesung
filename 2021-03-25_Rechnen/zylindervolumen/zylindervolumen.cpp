/*
 * filename:		zylindervolumen.cpp
 * path:			/zylindervolumen.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-25 12:09:53
 * date modified:	2021-03-25 12:09:55
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>

#define _USE_MATH_DEFINES // verwende die vordefinierten mathematischen Konstanten
#include <math.h> // Einbinde der C-Mathe-Bibliothek
/*
 * WICHTIG: auf richtige Reihenfolge achten
 * erst _USE_MATH_DEFINES, dann Bibliothek angeben
 */

#include <iomanip> // Bibliothek für Ein- Ausgabeformate

using namespace std;

int main(int argc, char *argv[]) {
	double radius, hoehe, volumen;
	// const double pi = 3.14159; // gewechselt auf M_PI aus math.h
	/*
	 * const kann nach deklaration nicht mehr verändert werden
	 * --> pi = pi + 1 wäre nicht mehr zulässig
	 */

	cout << "Willkommen zum Zylindervolumenprogramm" << endl;
	cout << "Geben Sie bitte den Radius und die Hoehe Ihres Zylinders ein: ";

	cin >> radius >> hoehe;

	volumen = radius * radius * hoehe * M_PI;

	cout << "Zylindervolument mit r = " << radius << ", h = " << hoehe << " --> V = " << setprecision(15) << volumen << endl;
	// setprecision(15) heisst: gib vor und nach dem Komma zusammen 15 Stellen aus

	return 0;
}