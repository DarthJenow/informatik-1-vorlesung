/*
 * filename:		input_addieren.cpp
 * path:			/input_addieren.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-25 11:42:28
 * date modified:	2021-03-25 11:43:19
 * Copyright (c)	2021 Simon Ziegler
 */

/*
 * AUFGABE
 * Definieren Sie eine Variablen namens zahl1 vom Typ int
 * und geben Sie dieser den Wert 100
 * Geben Sie zahl1 aus
 * Lesen Sie eine zweite Variable namen szahl2 vom Typ int vom Benutzer ein
 * Geben sie zahl2 aus
 * Berechnen Sie die Summe von zahl1 und zahl2 und geben Sie diese aus
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int zahl1 = 100, zahl2;
	/*
	 * Identisch zu:
	 * 
	 * int zahl1 = 100;
	 * int zahl2;
	 * 
	 * oder
	 * 
	 * int zahl1, zahl2;
	 * zahl1 = 100;
	 */

	cout << "zahl1 = " << zahl1 << endl;

	cout << "\nGeben sie einen Wert ein: zahl2 = ";

	cin >> zahl2;

	cout << "\nSie haben eingegeben: zahl2 = " << zahl2 << endl;

	cout << "\nzahl1 + zahl2 = " << zahl1 << " + " << zahl2 << " = " << zahl1 + zahl2 << endl;
	
	return 0;
}