/*
 * Dateiname:			zylinder_volumen.cpp
 * Datum:				2021-04-28 10:09:02
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-28 10:09:02
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double volumen (double r, double h);

int main(int argc, char const *argv[]) {
	double r, h, v;
	/**
	 * r, h, v sind lokale Variablen von main und sind nur dort bekannt
	 * r und h haben "zufällig" gleiche Namen wie in FUnktion volumen
	 */

	r = 2;
	h = 3;

	cout << "in main:" << endl;
	cout << "r = " << r << endl;
	cout << "h = " << h << endl;
	
	v = volumen(r, h);
	/**
	 * Funktionsaufruf
	 * hier wird beim Aufruf der Funktion volumen der Wert der lokalen Variablen r aus main
	 * in die lokale Variable r aus der Funktion volumen kopiert, h a analog
	 * bei return (Funktionsende) wird der Wert der lokalen Variablen erg aus der Funktion volumen
	 * zurückgegeben, das heißt hier der Variablen v zugewiesen
	 */

	cout << endl << "nach Funktionsaufruf in main:" << endl;
	cout << "r = " << r << endl;
	cout << "h = " << h << endl;
	cout << "V = " << v << endl;

	return 0;
}

double volumen (double r, double h) {
	/**
	 * was sind hier r und h?
	 * r und h sind hier lokale Variablen der Funktion volumen
	 * sind nur HIER bekannt, das heißt
	 * Änderungen von r und h in der Funktion werden NICHT in main gemacht
	 * hier im Beispiel gibt es also ZWEI VERSCHIEDNE Variablen namens r und
	 * 2 verschiedene Variablen namens h
	 */

	/**
	 * Alle lokalen Variablen , die in der Funktion definiert werden (im Kopf
	 * oder später im Rump), hier r, h, erg,  sind NUR innerhalb der Funktion bekannt,
	 * nicht außerhalb, es kann NICHT von außen auf sie zugegriffen werden
	 */

	r++;
	h++;

	cout << endl << "in Funktion volumen nach Erhoehen: " << endl;
	cout << "r = " << r << endl;
	cout << "h = " << h << endl;
	
	double erg; // lokale Variable erg

	erg = M_PI * r * r * h;
	
	return erg;
	/**
	 * Bei return --> Verlassen der Funktion: Alle in der Funktion volumen definierten
	 * lokalen Variablen verlieren ihre Gültigkeit --> sind unbekannt
	 */
}