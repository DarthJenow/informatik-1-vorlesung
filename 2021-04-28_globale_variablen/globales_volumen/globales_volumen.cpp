/*
 * Dateiname:			globales_volumen.cpp
 * Datum:				2021-04-28 11:03:59
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-28 11:03:59
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Volumenberechnung mit globalen Variablen
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <math.h>

#define _USE_MATHE_DEFINES
using namespace std;

double r, h, v; // globale Variablen für Radius, Höhe und Volumen

/**
 * FAUSTREGEL
 * Eigentlich sollten globale Variablen NUR verwendet werden, wenn in der Aufgabe
 * unbedingt erforderlich
 */

void volumen();

int main(int argc, char const *argv[]) {
	cout << "Geben sie Radius und hoehe eines Zylinders ein: ";
	cin >> r >> h; // Einlesen in globale Variablen
	
	volumen();

	cout << "Ein Zylinder mit r = " << r << " und  h = " << h << " hat ein Volumen von V = " << v << endl;

	return 0;
}

void volumen () {
	v = M_PI * r * r * h;
	/**
	 * alle Variablen sind global und damit bekannt!
	 * --> kein RÜckgabewert nötig, keine Übergabwerte nötig
	 */
}