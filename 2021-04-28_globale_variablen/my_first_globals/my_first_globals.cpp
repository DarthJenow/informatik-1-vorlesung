/*
 * Dateiname:			my_first_globals.cpp
 * Datum:				2021-04-28 10:48:22
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-28 10:48:22
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert globale Variablen
 * Eingabe:				-
 * Ausgabe:				Anzahl der Funktionsaufrufe, ...
 */

#include <iostream>
using namespace std;

int zaehler;
/**
 * Globale Variable zaehler, kann auch schon hier gleichzeitig initialisiert werden
 * WICHTIG
 * ganz am ANFANG definieren, damit überall bekannt!
 * VORSICHT --> kann ÜBERALL verändert werden!
 */

void linie(int l = 50);

int main(int argc, char const *argv[]) {
	zaehler = 0;
	/**
	 * Hier wird zaehler in main initialisiert, könnte auch bei
	 * der Defintion geschehen (oben)
	 */

	linie();

	cout << "Beginn von main" << endl;
	cout << "zaehler = " << zaehler << endl;

	linie();
	linie();

	cout << "Anzahl der Funktionsaufrufe von linie:" << endl;
	cout << "zaehler = " << zaehler << endl;
	
	zaehler *= 2; // globale Variable in main verändern

	cout << "Neuer Wert von zaehler" << endl;
	cout << "zahler = " << zaehler << endl;

	linie();

	cout << "Anzahl der Funktionsaufrufe von linie:" << endl;
	cout << "zaehler = " << zaehler << endl;

	return 0;
}

void linie(int l) {
	for (int i = 0; i < l; i++) {
		cout << '~';
	}

	cout << endl;

	zaehler++;
}