/*
 * Dateiname:			gutschein.cpp
 * Datum:				2021-04-28 11:14:32
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-28 11:14:32
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Erstellen Sie ein Programm, in dem Gutscheine eines Online-Händlers gekauft
 * und eingelöst werden können. Schreibe Sie dazu folgende Funktionen:
 * 
 * 1. gutscheinKaufen, die als Eingabe einen Betrag in Euro bekommt, diesen auf den Gutschein
 * schreibt und zur Kontrolle ausgibt
 * 
 * 2. gutscheinEinloesen, die als Eingabe einen Betrag in Euro bekommt. Sie soll prüfen, ob
 * der Betrag durch den Gutschein abgedeckt wird. Dann wird der Zahlungsbetrag vom Gutschein
 * abgezogen und der Bezahlvorgang sowie der neue Gutscheinbetrag ausgegeben
 */

double guthaben = 0;

void gutscheinKaufen(double betrag);
void gutscheinEinloesen(double betrag);

int main(int argc, char const *argv[]) {
	gutscheinKaufen(20);
	gutscheinEinloesen(5);
	gutscheinEinloesen(5);
	gutscheinEinloesen(5);
	gutscheinEinloesen(5);
	gutscheinEinloesen(5);


	return 0;
}

void gutscheinKaufen(double betrag) {
	if (betrag > 0) {
		cout << endl;
		cout << "~~~~~~~~~~~~~~~~~" << endl;
		cout << "Einkaufsgutschein" << endl;
		cout << "Wert: " << betrag << " Euro" << endl;
		cout << "~~~~~~~~~~~~~~~~~" << endl;
		cout << endl << "Ihr neues Guthaben betraegt" << guthaben << " Euro." << endl;

		guthaben += betrag;
	} else  {
		cout << "ERROR - Sie koennen nur positive Betraege einzahlen!" << endl;
	}
}

void gutscheinEinloesen(double betrag) {
	if (betrag > 0) {
		if (guthaben >= betrag) {
			guthaben -= betrag;

			cout << "Sie haben " << betrag << " Euro mit ihrem Gutschein-Guthaben bezahlt. Ihr neues Guthaben betraegt " << guthaben << " Euro." << endl;
		} else {
			cout << "ERROR - Guthaben nicht ausreichend!" << endl;
			cout << "Derzeit bieten wir leider keine Kredite an." << endl;
		}
	} else {
		cout << "ERROR - Sie koennen nur positive Betraege einleosen!" << endl;
	}
}