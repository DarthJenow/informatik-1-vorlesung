/*
 * Dateiname:			boolsche_funktionen.cpp
 * Datum:				/boolsche_funktionen.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-07 11:08:43
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Aussagen, ob 2 Zahlen gerade oder ungerade sind
 * Eingabe:				2 ganze Zahlen
 * Ausgabe:				Wahrheitswerte
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int zahl1, zahl2;
	bool vergleich1, vergleich2;

	cout << "Geben Sie 2 ganze Zahlen ein, die getestet werden sollen: ";
	cin >> zahl1 >> zahl2;

	vergleich1 = zahl1 % 2 == 0; // bekommt den Wahrheitswert der Behauptung, z1 sei eine gerade Zahl (d.h. zahl1 sei ohne Rest durch 2 teilbar)
	vergleich2 = zahl2 % 2 == 0;

	cout.setf(cout.boolalpha);

	cout << zahl1 << " ist eine gerade Zahl ist " << vergleich1 << endl;
	cout << zahl2 << " ist eine gerade zahl ist " << vergleich2 << endl;

	bool c; // Definition weiterer boolschen Ergebnisvariablen c

	// UND
	c = vergleich1 && vergleich2;
	/*
	 * c bekommt den Wahrheitswert vergleich1 UND vergleich2, das heißt
	 * sowohl vergleich1 als auch vergleich2 müssen beide wahr (erfüllt) sein
	 */
	cout << endl << "UND" << endl;
	cout << zahl1 << " und " << zahl2 << " sind beide gerade Zahlen ist " << c << endl;

	// ODER
	c = vergleich1 || vergleich2;
	/*
	 * c bekommt den Wahrheitswert verlgleich1 ODER vergleich2, das heißt
	 * meindestens eine der beiden Variablen vergleich1, vergleich2 muss wahr sein
	 */
	cout << endl << "ODER" << endl;
	cout << "Von " << zahl1 << " und " << zahl2 << " ist mindestens eine Zahl gerade ist " << c << endl;

	// X-ODER
	c = vergleich1 != vergleich2;
	// Alternativen
	// c = (vergleich1 || vergleich2) && !(vergleich1 && vergleich2);
	// c = vergleich1 && !vergleich2 || !vergleich1 && vergleich2; // UND ist stärker als ODER (vgl. Punkt-vor-Strich)
	// c = (vergleich1 + vergleich2) % 2;
	/*
	 * c bekommt den Wahrheitswert entweder vergleich1 oder vergleich2 zugewiesen, das heißt
	 * c ist genau dann wahr, wenn nur eine der beiden Werte vergleich1, vergleich2 wahr ist.
	 */
	/*
	 * (a || b) && !(a && b) kann umgeformt werden zu:
	 * (a || b) && (!a || !b) // ! in Klammer reinziehen --> !( && ) --> (! || !) und umgekehrt
	 * (a && (!a || b))  || (b && (!a || !b)) // Letzte Klammer in erster Klammer reingezogen
	 * (a && !a || a && !b) || (b && !a || b && !b) // Innere Klammer aufgelöst
	 * (false || a && !b) || (b && !a || false) // false mit oder angebunden --> weglassen (wie Addition mit 0)
	 * (a && !b) || (b && !a)
	 * a && !b || !a && b
	 */

	cout << endl << "X-ODER" << endl;
	cout << "Von " << zahl1 << " und " << zahl2 << " ist genau eine Zahl gerade ist " << c << endl;

	// NICHT
	c = !vergleich1;
	/*
	 * c bekommt den Wahrheitswert NICHT vergleich1 zugewiesen, das heißt
	 * ist vergleich1 true -> c bekommt den Wert false und umgekehrt
	 */
	cout << endl << "NICHT" << endl;
	cout << zahl1 << " ist keine (nicht eine) gerade Zahl ist " << c << endl;

	// if (vergleich1 && vergleich2) {
	// 	cout << "Beide Zahlen sind gerade!" << endl;
	// } else if (vergleich2 || vergleich2) {
	// 	cout << "Nur eine der beiden Zahlen ist gerade!" << endl;
	// } else {
	// 	cout << "Keine der Zahlen ist gerade!" << endl;
	// }

	return 0;
}
