/*
 * Dateiname:			inch_cm.cpp
 * Datum:				/inch_cm.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-07 09:57:16
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	/*
	 * AUFGABE
	 * Schreiben Sie ein Programm, das Inches in Zentimeter umwandelt (und umgekehrt).
	 * Der Umwandlungsfaktor ist 25.4.
	 * Der Benutzer soll eine Zahl eingeben und eien Buchnstaben mit i für Umwandeln in Inches, c für
	 * Umwandeln in Zentimeter, in allen anderen Fällen wird ausgegeben, dass keien Berechnung erfolgt ist.
	 * Überlegen Sie zuerst, welche Variablen und Konstanten Sie brauchen sowie was ein- und ausgegeben wird
	 */

	double input_laenge, output_laenge;
	char einheit;
	const float zoll_laenge = 25.4; // Länge eines Zolls in Millimeter

	cout << "Geben Sie eine Länge ein, die sie umrechnen möchten und einen Buchstaben, in welche Einheit Sie konvertieren möchten." << endl;
	cout << "'i' --> In Zoll umwandeln" << endl;
	cout << "'m' --> In Millimeter umwandeln" << endl;
	cout << "l = ";
	
	cin >> input_laenge >> einheit;

	if (einheit == 'i') {
		output_laenge = input_laenge / zoll_laenge;
		
		cout << input_laenge << "mm = " << output_laenge << "\"" << endl;
	} else if (einheit == 'm') {
		output_laenge = input_laenge * zoll_laenge;

		cout << input_laenge << "\" = " << output_laenge << "mm" << endl;
	} else {
		cout << "Sie haben keine gueltige Einheit eingegeben." << endl;
	}

	return 0;
}
