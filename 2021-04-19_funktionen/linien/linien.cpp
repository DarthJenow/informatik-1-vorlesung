/*
 * Dateiname:			linien.cpp
 * Datum:				2021-04-19 08:14:30
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-19 08:14:30
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Linien aus Zeichen schreiben lassen per Funktion
 * Eingabe:				Zeichen
 * Ausgabe:				Linie aus Zeichen
 */

/**
 * AUFGABE1
 * Geben Sie nun 3 Linien des Typs linie2 aus, eine mit #, dann mit*, dann mit 6
 */

#include <iostream>
using namespace std;

/**
 * Hier kommen die Funktionsbekanntmachungen (Deklarationen) hin
 * das heißt, nur den Funktionskopf schreiben
 */

void linie();
void linie2(char z);
void linie3(char z, int l);
/**
 * Deklaration einer Funktion namens linie, ohne Rückgabetyp
 * und ohne Eingabewerte
 */
/**
 * allgemein:
 * Rückgabetyp Funktionsname (Typ_Eingabewert1 Name_Eingabewert1, ...)
 * 
 * hier: void --> kein Rückgabewert
 * --> Funktion macht NUR was, gibt NICHTS zurück
 * keine Eingabewerte --> Liste bleit mleer mit ()
 */

int main(int argc, char const *argv[]) {
	linie();
	/**
	 * Aufruf einer Funktion ohne Rückgabewert
	 * nur Funktionsame mit Liste der Übergabewerte,
	 * die ist hier leer, das heißt (), muss aber trotzdem angegeben werden
	 */
	
	for (int i = 0; i < 10 ; i++) {
		/**
		 * Gib 10 Linien untereinander aus, das heißt rufe 10 mal hintereinander
		 * linie auf
		 */

		linie();
	}

	char zeichen;

	cout << "Willkommen zum Linienprogramm!" << endl;
	cout << "Geben Sie ein Zeichen für eine Linie ein: ";
	cin >> zeichen;

	linie();

	cout << "Ihre Linie" << endl;

	linie2(zeichen);
	/**
	 * Hier wir die Funktion linie2 mit einem Eingabewert aufgerufen
	 * das kann eien Variable, ein konkreter Wert oder z.B. eine Rechnung sein
	 */

	linie2('#');
	linie2('*');
	linie2('6');

	linie();
	
	// Variable zeichen von oben wiederverwenden
	int anzahl;

	cout << "Geben Sie ein Zeichen und die Laenge der Linie aus diesem Zeichen ein: ";
	cin >> zeichen >> anzahl;

	linie3(zeichen, anzahl);

	return 0;
}

/**
 * Hier kommen die FUnktionsdefinitionen hin, das heißt die Beschreibung, was die Funktionen
 * machen sollen
 */

/**
 * allgemein:
 * void Funktionsname(Eingabewerte) {
 * 		Anweisungen
 * }
 */

void linie() {
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}

void linie2(char z) { // Funktion verlang bei aufruf die Übergabe eiens Zeichens, z ist Platzhalter
	/**
	 * VORSICHT!
	 * Platzhalter z ist nur HIER, in der Funktion linie2, bekannt!
	 * --> z ist LOKALE  Variable der Funktion linie2
	 * in main ist z NICHT bekannt
	 */

	for (int i = 0; i < 100; i++) {
		cout << z;
	}
	// Funktion mithilfe des Platzhalters z beschrieben

	cout << endl;
}

void linie3(char z, int l) {
	for (int i = 0; i < l; i++) {
		cout << z;
	}

	cout << endl;
}