// Willkommensprogramm
// heißt Kommentar in einer Zeile oder nach eeiner Codezeile

/* Dieser Kommentar
geht über
mehrere Zeilen */

// Programmkopf oder Deklarationsteil
// hier kommt alles hin, was das Programm zur Ausführung wissen muss

#include <iostream> // Bibliothek für Ein- und Ausgabebefehle einbinden
using namespace std; // Verwende den Namensraum std (std steht für standard)


//------------------Programmrumpf
// jetzt kommt die Hauptfunktion main
// WICHTIG! Pro Projekt darf nur eine Datei mit der Funktion main vorkommen!

int main() {
	cout << "Herzlich Willkommen zur Vorlesung" << "foobar" << endl;
	
	// Ausgabe mti Befehl cout und Ausgabeoperator <<
	// es können mit << mehrere, auch verschiedene, Ausgaben aneinandergehängt werden
	// Text imer in " "
	// endl (end line) heißt Zeilensprung

	// WICHTIG
	// ; heißt immer ENDE des Befehls und wird auch am Ende eines jeden Befehls benötigt
	
	return 0;
} // Ende der Anweisungsfolge