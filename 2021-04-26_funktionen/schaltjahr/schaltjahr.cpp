/*
 * Dateiname:			schaltjahr.cpp
 * Datum:				2021-04-26 09:09:48
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-26 09:09:48
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Schreiben Sie eien Funktion die zurückgibt, ob eine übergebene Jahreszahl ein
 * Schaltjahr ist
 * Lesen Sie in main die Jahreszahl vom Benutzer ien und geben Sie einen Aussagesatz, ob es ein Schaltjahr ist, aus
 */

bool ist_schaltjahr(int jahr);

int main(int argc, char const *argv[]) {
	int jahr_input;

	cout << "Geben Sie eien Jahreszahl ein: ";
	cin >> jahr_input;

	if (jahr_input < 0) {
		cout << "Flasche Eingabe, Programmende!" << endl;
	} else {
		cout.setf(cout.boolalpha);

		cout << jahr_input << " ist ";
		if (ist_schaltjahr(jahr_input)) {
			cout << "ein";
		} else {
			cout << "kein";
		}

		cout << " Schaltjahr" << endl;
	}
	
	return 0;
}

bool ist_schaltjahr(int jahr) {
	return jahr % 4 == 0 && !(jahr % 400 == 0) || jahr % 400 == 0;
}