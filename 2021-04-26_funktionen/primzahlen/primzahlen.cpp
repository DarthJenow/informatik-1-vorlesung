/*
 * Dateiname:			primzahlen.cpp
 * Datum:				2021-04-26 09:10:18
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-26 09:10:18
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet alle Primzahlen zwischen 1 und 100
 * Eingabe:				-
 * Ausgabe:				Primzahlen
 */

#include <iostream>
using namespace std;

bool ist_prim (int zahl);

int main(int argc, char const *argv[]) {
	for (int i = 0; i <= 100; i++) {
		if (ist_prim(i)) {
			cout << i << endl;
		}
	}
	
	return 0;
}

bool ist_prim (int zahl) {
	for (int i = 2; i < zahl / 2; i++) {
		if (zahl % i == 0) {
			return false;
		}
	}

	return true;
}