/*
 * Dateiname:			sortieren.cpp
 * Datum:				2021-06-07 09:21:05
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-07 09:21:05
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE:
 * Schreiben Sie eine Funktion void sortiere, die 3 eingegebene double-Variablenwerte
 * aufsteigend der Größe nach sortiert
 * 
 * Lesen Sie in main 3 DoubleVariablen ein,
 * rufen sie sortiere auf
 * Geben Sie die Variablen aus
 */

void sortiere(double * a, double * b, double * c);
void tausche(double * a, double * b);

int main(int argc, char const *argv[]) {
	double a, b, c;

	cout << "Geben Sie drei Zahlen ein (Typ: double): ";
	cin >> a >> b >> c;

	sortiere(&a, &b, &c);

	cout << "\nDie Zahlen in sortierter Reihenfolge:" << endl;
	cout << a << '\t' << b << '\t' << c << endl;
	
	return 0;
}

void sortiere(double * a, double * b, double * c) {
	if (*a > *b) {
		tausche(a, b);
	}
	if (*b > *c) {
		tausche(b, c);
	}
	if (*a > *b) {
		tausche(a, b);
	}
}

void tausche (double * a, double * b) {
	*a += *b;
	*b = *a - *b;
	*a -= *b;
}