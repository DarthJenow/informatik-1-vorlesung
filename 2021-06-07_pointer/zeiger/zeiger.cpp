/*
 * Dateiname:			zeiger.cpp
 * Datum:				2021-06-07 08:14:41
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-07 08:14:41
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	int z = 42, a;
	/**
	 * Definiert eine Variable Namens z vom Typ int und weißt dieser den Wert 42 zu.
	 * Das heißt, zur Programmbeginn ist ein Speicherplatz von 4 zusammenhängenden Byte
	 * dafür reserviert (int --> 4 Byte), der als ganze Zahl interpretiert wird
	 * Speicherplatz ist über den Namen der der Variablen ansprechbar
	 */

	cout << "z: " << z << endl;
	cout << "Adresse von z: " << &z << endl;
	/**
	 * & ist der Adressoperator, liefert Adresse einer Variablen
	 * Erstes Byte des Speicherplatzes der Variablen z
	 */

	int *contZ = &z;
	/**
	 * Definition eines Adresscontainers (Zeigers / Pointers) namens contZ
	 * Bei der Definition  * vor den Variablennamen schreiben
	 * --> Variable ist vom Typ Adresscontainer (Zeiger)
	 * 
	 * Typ VOR * beziehungsweise Adresscontainer: gibt an, welche Art von Speicherplatz an der Adresse,
	 * die im Adresscontainer gespeichert wird, hier:
	 * int --> Speicherplatz hat größe 4 Byte, als int zu lesen
	 * 
	 * Hier wird der variablen conZ vom Typ Adresscontainer die Adresse von z zugewiesen
	 * --> contZ wird mit der Adresse von z bei der Definition initialisiert
	 */
	
	cout << "Inhalt des Adrescontainers (Zeigers) contZ: " << contZ << endl;
	/**
	 * Hier den Adresscontainer wie normale Variable behandeln
	 * Inhalt des Adresscontainers ausgeben --> Adresscontainernamen mit cout ausgeben
	 */

	cout << "Inhalt der Speicherzelle (SPZ), deren Adresse in contZ steht: " << *contZ << endl;
	/**
	 * *contZ gibt den INHALT  der Speicherzelle, deren Adresse im Container (Zeiger) gespeichert ist, aus.
	 * Sprechweise: contZ zeigt auf die Speicherzelle oder zeigt auf die Variable z
	 * 
	 * VORSICHT:
	 * Doppelfunktion von *
	 * 1. Bei der Definition von Zeigern (Adresscontainern): DatentypSpeicherplatz *Zeigername;
	 * 2. Bei Zugriff auf den INHALT des Speicherplatzes, dessen Adresse im Zeiger gespeichert ist
	 */

	*contZ += 7; // aktives Verändern des Speicherplatzinhaltes, *-Operator

	cout << "Inhalt der Speicherzelle, deren Adresse in contZ steht: " << *contZ << endl;
	cout << "z: " << z << endl;

	int y = 15;
	cout << "\ny: " << y << ". Adresse von y: " << &y << endl;
	int * magnetkarte;

	magnetkarte = &y;

	cout << "Adresse,die im Container magnetkarte steht: " << magnetkarte << endl;
	cout << "INHALT der SPZ, deren Adresse im Container Magnetkarte steht: " << *magnetkarte << endl;

	magnetkarte = contZ; // Zeiger (Adresscontainer) Magnetkarte bekommt Adresse, die in contZ steht, zugewiesen

	cout << "\nAdresse, die in Zeiger Magnetkarte steht: " << magnetkarte << endl;
	cout << "INHALT der SPZ, deren Adresse im Pointer Magnetkarte steht: " << *magnetkarte << endl;

	return 0;
}