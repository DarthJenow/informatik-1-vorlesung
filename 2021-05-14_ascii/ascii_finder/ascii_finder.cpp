/*
 * Dateiname:			ascii_finder.cpp
 * Datum:				2021-05-17 11:54:13
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-17 11:54:13
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	/**
	 * FRAGE
	 * Welches Zeichen gehört zu einer eingegeben ASCII-Nummer?
	 */

	int code;
	char zeichen;

	cout << "Geben Sie einen ASCII-Code ein: ";
	cin >> code;

	zeichen = code;

	cout << code << " --> \"" << zeichen << "\"" << endl;

	cout << endl << "Geben Sie ein Zeichen ein: ";
	cin >> zeichen;

	code = zeichen;

	cout << "\"" << zeichen << "\" --> " << code << endl;
	
	return 0;
}