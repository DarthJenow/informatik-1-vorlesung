/*
 * Dateiname:			buchstabenwandeln.cpp
 * Datum:				2021-05-14 08:49:31
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-14 08:49:31
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Lesen Sie solange vom Zeichen vom Benutzer ein, solange es KEIN # ist
 * - ist das Zeichen ein Großbuchstabe, geben Sie es aus, wandeln es in einen Kleinbuchstaben und geben diesen aus
 * - Verfahren Sie genauso umgekehrt mit Kleinbuchstaben
 * - Ist das Zeichen eine Ziffer, geben Sie das Zeichen aus und schreiben "ist eine Ziffer"
 * - In allen anderen Fällen geben sie aus: "sonstiges Zeichen"
 */

int main(int argc, char const *argv[]) {
	char input_char, convert_char;
	int converter;

	cout << "Willkommen zum myterioesen Zeichenwandler!" << endl;

	do {
		cout << endl << "Geben Sie ein Zeichen ein: ";
		cin >> input_char;

		cout << input_char;

		if (input_char >= '0' && input_char <= '9') { // ist Ziffer
			cout << " ist eine Ziffer" << endl;
		} else if (input_char >= 'A' && input_char <= 'Z') { // ist Großbuchstabe
			cout << " ist ein Großbuchstaben. Als Kleinbuchstabe: " << (char) (input_char + 32) << endl;
		} else if (input_char >= 'a' && input_char <= 'z') { // ist Kleinbuchstabe
			cout << " ist ein Kleinbuchstabe. Als Großbuchstabe: " << (char) (input_char - 32) << endl;
		} else {
			cout << " ist ein sonstiges Zeichen" << endl;
		}
	} while (input_char != '#');
	
	return 0;
}