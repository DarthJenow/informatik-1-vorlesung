/*
 * Dateiname:			durchschnittszeichen.cpp
 * Datum:				2021-05-14 09:20:53
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-14 09:20:53
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	char input_char;
	int64_t average_char = 0;
	int char_count = 0;

	do {
		cout << "Geben Sie ein Zeichen ein: ";
		cin >> input_char;
		cout << (int) input_char << endl;

		if (input_char != '#') {
			average_char += input_char;
			char_count++;

			input_char = (average_char / char_count) % 127;

			cout << "Das Durchschnittszeichen lautet: '" << input_char << "', es ist das " << char_count << ". Zeichen." << endl;
		} else { 
			cout << "'#' eingegeben. Programm wird beendet!" << endl;
		}

	} while (input_char != '#');
	
	return 0;
}