/*
 * Dateiname:			cosinus.cpp
 * Datum:				2021-05-03 09:18:33
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-03 09:18:33
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Cosinus per Taylorpolynom
 * Eingabe:				Winkel in Grad
 * Ausgabe:				Cosinus
 */

#include <iostream>
#include <iomanip>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double deg2rad(double w); // Winkel von Grad in Bogenmaß umwandeln
long fak(int n); // Fakultät einer ganzen Zahl
double pot(double b, int e); // Potenz mit natürlichem Exponent

double taylor_cos(double w); // Cosinus durch Taylorpolynom berechnen

int main(int argc, char const *argv[]) {
	double winkel;

	cout << "Geben Sie einen Winkel zur Cosinus-Berechnung in Grad ein: ";
	cin >> winkel;
	
	cout << "Ihr Winkel im Grad: " << winkel << endl;
	cout << "Ihr Winkel im Bogenmaß: " << deg2rad(winkel) << endl;

	cout << "cos(" << winkel << "°) = " << cos(deg2rad(winkel)) << endl;
	
	return 0;
}

double deg2rad(double w) {
	return w / 180 * M_PI;
}

/**
 * Berechnet die Fakultät
 * 
 * NEU:
 * int erzeugt overflow
 * --> stattdessen long genutzt
 */
long fak(int n) {
	long res = 1;

	for (int i = 2; i <= n; i++) {
		res *= i;
	}

	return res;
}

double pot(double b, int e) {
	double res = 1;

	for (int i = 1; i <= e; i++) {
		res *= b;
	}

	return res;
}

double taylor_cos(double w) {
	/**
	 * Taylorpolynom --> Aufsummieren in einer Schleife
	 */

	double cos_poly = 0, cos_last, diff;

	for (int n = 0; true; n++) {
		cos_last = cos_poly;

		cos_poly += pot(-1, n) * pot(w, 2*n) / fak(2*n);

		diff = cos_poly - cos_last;
		if (diff < 0) {
			diff *= -1;
		}

		// // Kontrollausgabe
		// cout << n << ". Taylorpolynom: " << fixed << setprecision(15) << cos_poly << endl;

		if (diff < 0.0000001) {
			break;
		}
	}

	return cos_poly;
}