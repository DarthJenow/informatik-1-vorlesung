/*
 * Dateiname:			bool_funktion.cpp
 * Datum:				2021-04-23 08:50:37
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-23 08:50:37
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Kreisflächenvergleichen
 * Eingabe:				radien
 * Ausgabe:				Aussagen
 */

#include <iostream>
#include <math.h>

#define _USE_MATH_DEFINES
using namespace std;

double kreis_flaeche (double radius);
bool ist_kleiner (double kreis1, double kreis2);

int main(int argc, char const *argv[]) {
	double r1, r2, A1, A2;
	bool A_vergleich;

	cout << "Willkommen zum Kreisevergleichen!" << endl;
	cout << "Geben Sie den Radius von Kreis1 und Kreis2 ein: ";
	cin >> r1 >> r2;

	A1 = kreis_flaeche(r1);
	A2 = kreis_flaeche(r2);
	A_vergleich = ist_kleiner(A1, A2);

	cout << "Der Kreis1 mit dem Radius r = " << r1 << " hat eine Fläche von " << A1 << endl;
	cout << "Der Keis2 mit dem Radius r = " << r2 << " hat eine Fläche von " << A2 << endl;

	if (A_vergleich) {
		cout << "Kreis1 ist kleiner als Kreis2." << endl;
		cout << "A_1 < A2" << endl;
		cout << A1 << " < " << A2 << endl;
	} else {
		cout << "Kreis1 ist größer oder gleichgroß wie Kreis2." << endl;
		cout << "A_1 >= A2" << endl;
		cout << A1 << " >= " << A2 << endl;
	}
	
	return 0;
}


double kreis_flaeche (double radius) {
	return radius * radius * M_PI;
}

// // 1. Variante
// bool ist_kleiner (double kreis1, double kreis2) {
// 	bool ergebnis;

// 	if (kreis1 < kreis2) {
// 		ergebnis = true;
// 	} else {
// 		ergebnis = false;
// 	}

// 	return ergebnis;
// }

// // 2. Variante
// bool ist_kleiner (double kreis1, double kreis2) {
// 	bool ergebnis = false;

// 	if (kreis1 < kreis2) {
// 		ergebnis = true;
// 	}

// 	return ergebnis;
// }

// // 3. Variante
// bool ist_kleiner (double kreis1, double kreis2) {
// 	if (kreis1 < kreis2) {
// 		return true;
// 	} else {
// 		return false;
// 	}
// }

// 4. Variante
bool ist_kleiner (double kreis1, double kreis2) {
	return kreis1 < kreis2;
}