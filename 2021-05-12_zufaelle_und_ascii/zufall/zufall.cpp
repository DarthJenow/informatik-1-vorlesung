/*
 * Dateiname:			zufall.cpp
 * Datum:				2021-05-12 10:43:27
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-12 10:43:27
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Erzeuge Zufallszahlen
 * Eingabe:				
 * Ausgabe:				zufällige Zahlen
 */

#include <iostream>
#include <stdlib.h> // Bibliothek mit Zufallszahlengenerator
#include <ctime> // Bibliothek für Zeitfunktionen
using namespace std;

int main(int argc, char const *argv[]) {
	srand(2345); // Initilaisierung des Zufallsgenerators
	/**
	 * VORSICHT
	 * Startwert in srand bestimmt die Zufalszahlen!
	 * Sollte möglichst individuell sein!
	 * z.B. Handynummer, Steuer-ID-Nummer, Sozialversicherungsnummer
	 */

	srand(clock()); // Initilaisierung mit der bisher verbrauchten CPU-Zeit

	int zahl;

	// Ausgabe von 15 Zufallszahlen hintereinander
	cout << "15 Zufallszahlen" << endl;
	for (int i = 1; i <= 15; i++) {
		zahl = rand();
		cout << zahl << endl;
	}

	cout << endl << "Würfel" << endl;

	for (int i = 1; i <= 15000; i++) {
		zahl = rand() % 6 + 1;
		
		cout << zahl << endl;
	}
	
	/**
	 * AUFGABE
	 * Erzeugen Sie 10 mal hintereinander Zufallszahlen im Interval [5, 40]
	 * Geben Sie diese aus
	 */

	cout << endl << "Intervall [5, 40]" << endl;

	for (int i = 1; i <= 10; i++) {
		zahl = rand() % 36 + 5;

		cout << zahl << endl;
	}

	return 0;
}