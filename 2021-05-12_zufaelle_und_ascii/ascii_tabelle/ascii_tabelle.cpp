/*
 * Dateiname:			ascii_tabelle.cpp
 * Datum:				2021-05-12 11:10:02
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-12 11:10:02
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Gibt Zeichen und ihren dazugehörigen ASCII-Code aus
 * Eingabe:				-
 * Ausgabe:				s.o.
 */

#include <iostream>
using namespace std;

void asciiTable();

int main(int argc, char const *argv[]) {
	asciiTable();

	return 0;
}

void asciiTable() {
	char zeichen;

	for (int c_no = 32; c_no < 128; c_no++) {
		zeichen = c_no;
		/**
		 * weist einem char einen int zu
		 * hier char zeichen bekommt den Wert des integers c_no
		 * --> Rechner sucht Nummer (Zahl) in ASCII-Tablle und
		 * weist das dort stehende Zeichen der Variablen Zeichen zu
		 */

		cout << c_no << ": " << zeichen << "\t";
		// \t steht für Tab-Sprung

		if (c_no % 8 == 7) { // Zeilensprung nach 8 Ausgaben
			cout << endl;
		}
	}
}