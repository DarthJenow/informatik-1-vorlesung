/*
 * Dateiname:			team.cpp
 * Datum:				2021-06-02 11:53:28
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-02 11:53:28
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <string>
using namespace std;

/**
 * AUFGABE
 * Erstellen Sie in maine ein Array aus Strings, in dem Sie Teams abspeicher können (nur Nachnamen ohne Leerzeichen)
 * Die Teamgröße kann schwanken (was bedeutet das?)
 * 
 * Schreiben Sie folgende Funktionen
 * - einlesen, zum Einlesen der Teammitglieder
 * - ausgabe, zum Ausgeben des Teams
 * - sortieren
 */

void einlesen(string team[]);
void ausgabe(string team[]);
void sortieren(string team[]);

int anzahl_mitglieder;

int main(int argc, char const *argv[]) {
	string team[INT16_MAX];
	
	cout << "Wieviele Mitglieer hat ihr Team? ";
	cin >> anzahl_mitglieder;

	einlesen(team);

	ausgabe(team);

	sortieren(team);

	cout << "\nJetzt in sortiert:" << endl;

	ausgabe(team);
	
	return 0;
}

void einlesen(string team[]) {
	cout << "Geben Sie die Namen der Mitglieder ein" << endl;;
	for (int i = 0; i < anzahl_mitglieder; i++) {
		cout << i + 1 << ". Mitglied: ";
		// getline(cin, team[i]);
		cin >> team[i];
	}
}

void ausgabe(string team[]) {
	cout << "Die Teammitglieder sind:" << endl;
	for (int i = 0; i < anzahl_mitglieder; i++) {
		cout << team[i] << endl;
	}
}

void sortieren(string team[]) {
	string temp;
	for (int i = anzahl_mitglieder - 1; i > 0; i--) {
		for (int j = 0; j < i; j++) {
			if (team[j] > team[j + 1]) {
				temp = team[j];
				team[j] = team[j + 1];
				team[j + 1] = temp;
			}
		}
	}
}