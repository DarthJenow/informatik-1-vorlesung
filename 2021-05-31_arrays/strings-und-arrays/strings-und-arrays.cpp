/*
 * Dateiname:			strings-und-arrays.cpp
 * Datum:				2021-06-02 12:24:10
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-02 12:24:10
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Ersetzt alle kleinen Vokale im String durch *
 * Eingabe:				Zeichenkette
 * Ausgabe:				Veränderte Zeichenkette
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	string kette;

	cout << "Geben Sie ihre Zeichenkette ein: ";
	getline(cin, kette);

	for (int i = 0; i < kette.length(); i++) {
		if (kette[i] == 'a' || kette[i] == 'e' || kette[i] == 'i' || kette[i] == 'o' || kette[i] == 'u') {
			kette[i] = '*';
		}

		/**
		 * Kette ist auch ein Array vom Typ char
		 * --> Indexoperator[] kann genauso verwendet werden, anstelle von at und replace
		 */
	}

	cout << "Der veraenderte String sieht nun so aus: " << kette << endl;
	
	return 0;
}