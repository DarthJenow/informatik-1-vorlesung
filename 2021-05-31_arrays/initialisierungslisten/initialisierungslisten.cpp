/*
 * Dateiname:			initialisierungslisten.cpp
 * Datum:				2021-06-02 03:50:49
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-02 03:50:49
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	// char gruss[3][4] = { 'G', 'u', 't', 'e', 'n', ' ', 'M', 'o', 'r', 'g', 'e', 'n' };
	char gruss[3][4] = { 
		{'G', 'u', 't', 'e'}, 
		{'n', ' ', 'M', 'o'}, 
		{ 'r', 'g', 'e', 'n'}
	};
	/**
	 * Zur bebesseren Übersicht:
	 * Zeielen mittels { } gruppieren (optional: und in verschiedene Zeilen schreiben)
	 */

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << gruss[i][j];
		}
		cout << endl;
	}

	/**
	 * AUFGABE:	
	 * Definieren und initialisieren Sie eine 4x3-Matrix vom Typ int folgendermaßen:
	 * 1. Zeile nur 3en,
	 * 2. Zeile nur 0en,
	 * 3. Zeile 0 0 4,
	 * 4. Zeile 5 0 0
	 * 
	 * Verwenden Sie so wenige Werte in der Initialisierungsliste wie möglich!
	 * Geben Sie ihr Array aus
	 */

	cout << "\nneues Array mit Zahlen" << endl;

	int zahlen[4][3] = {
		3, 3, 3,
		{},
		{0, 0, 4},
		{5}
	};
	// Zeile nur mit Nullen mit {}

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 3; j++) {
			cout << zahlen[i][j];
		}
		cout << endl;
	}
	
	return 0;
}