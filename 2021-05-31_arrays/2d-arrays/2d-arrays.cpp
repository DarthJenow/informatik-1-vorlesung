/*
 * Dateiname:			2d-arrays.cpp
 * Datum:				2021-06-02 12:33:00
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-02 12:33:00
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

void ausgabe(int array[3][4]);
void eingabe(int array[3][4]);

int main(int argc, char const *argv[]) {
	int myarray[3][4] = { 0 };
	/**
	 * Deifintion eines 2D-Arrays vom Typ int namens meinFeld
	 * mit 3 Zeilen und 4 Spalten, sowie gleichzeitiger
	 * Initilisierung aller Werte mit 0 mittels Initialisierungsliste
	 * 
	 * ALLGEMEIN:
	 * Typ Arrayname[Zeilenanzahl][Spaltenanzahl];
	 * Zeilen zuerst, Spalten später
	 * 
	 * WICHTIG:
	 * 2D-Arrays sind immer mit Doppelschleifen verheiratet, erst über die Zeilen, dann über die Spalten
	 */

	// Ausgabe
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << myarray[i][j] << '\t';
		}

		cout << endl;
	}

	// Einzelne Werte  gezielt verändern
	myarray[2][1] = 4;
	myarray[1][2] = 3;
	myarray[0][0] = 187;
	myarray[0][3] = 15;

	// Ausgabe mittels FUnktion
	cout << "\nGeanderte Matrix:" << endl;
	ausgabe(myarray); // Nur Arrayname, keinen Typ, keine Indizes
	
	/**
	 * AUFGABE:
	 * Erstellen sie eine Eingabefunktion für ein 3x4-Array
	 * Rufen Sie diese in main auf und geben Sie dann mittels Funktion das neue Array aus
	 */

	cout << "\nGeben Sie Daten fuer ein neues Array ein:" << endl;
	eingabe(myarray);

	cout << "\nDein Array lautet nun:" << endl;
	ausgabe(myarray);

	return 0;
}

void ausgabe(int array[3][4]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << array[i][j] << '\t';
		}

		cout << endl; 
	}
}

void eingabe(int array[3][4]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << "Wert fuer [" << i << "][" << j << "] = ";
			cin >> array[i][j];
		}
	}
}