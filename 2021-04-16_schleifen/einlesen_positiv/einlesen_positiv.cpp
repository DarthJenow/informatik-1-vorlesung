/*
 * Dateiname:			einlesen_positiv.cpp
 * Datum:				2021-04-16 10:10:17
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-16 10:10:17
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				positive Zahl einlesen
 * Eingabe:				Zahl
 * Ausgabe:				positive Zahl
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	double zahl;

	do {
		cout << "Geben Sie eine positive Zahl ein: ";
		cin >> zahl;
	} while (zahl < 0);

	cout << "Ihre Zahl " << zahl << " ist positiv!" << endl;
	
	return 0;
}