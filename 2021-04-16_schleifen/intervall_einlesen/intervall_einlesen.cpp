/*
 * Dateiname:			intervall_einlesen.cpp
 * Datum:				2021-04-16 10:20:28
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-16 10:20:28
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Zahlen innerhalb eines Intervalls einlesen
 * Eingabe:				ganze Zahlen
 * Ausgabe:				Zahlen im Intervall
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	int von = -3;
	int bis = 24;
	int zahl;

	do {
		cout << "Geben Sie ein Zahl im Intervall [" << von << ", " << bis << " ] ein, um der Schleife zu entkommen!" << endl;
		cin >> zahl;
	} while (! (zahl < von || zahl > bis));

	cout << "Sie sind mit der Zahl " << zahl << " der Schleife entkommen!" << endl;
	
	return 0;
}