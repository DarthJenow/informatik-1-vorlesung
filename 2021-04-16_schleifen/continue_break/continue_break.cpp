/*
 * Dateiname:			continue_break.cpp
 * Datum:				2021-04-16 09:21:12
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-16 09:21:12
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Einlesen ganzer Zahlen, positives Aufsummieren, Ende mit 0
 * Eingabe:				ganze Zahlen
 * Ausgabe:				Teilsummen
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int zahl, summe = 0;

	while (true) { // Endlos-Schleife, Bedingung ist immer erfüllt 	
		cout << "Ganze Zahl eingeben: ";
		cin >> zahl;

		if (zahl == 0) {
			break; // Abbruch der Schleife
		}

		if (zahl < 0) {
			continue;
			/*
			 * Ignoriert alle weiteren Befehle bis zum Schleifenende
			 * springt dann wieder an den Schleifenanfang
			 */
		}

		/**
		 * Wenn das Programm hier ankommt, dann ist
		 * zahl auf jeden Fall positiv
		 */

		summe += zahl;
		cout << "Neue Teilsumme: " << summe << endl;
	}		
	
	return 0;
}
