/*
 * Dateiname:			intervall_einlesen.cpp
 * Datum:				2021-04-16 10:28:23
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-16 10:28:23
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Konsonanten einlesen
 * Eingabe:				char
 * Ausgabe:				Konsonant
 */

#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
	char input;

	do {
		cout << "Geben Sie einen Buchstaben ein: ";
		cin >> input;
	} while (input == 'a' || input == 'e' || input == 'i' || input == 'o' || input == 'u');

	cout << "Sie haben einen Konsonanten eingeben: " << input << endl;
	
	return 0;
}