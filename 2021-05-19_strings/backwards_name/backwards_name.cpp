/*
 * Dateiname:			backwards_name.cpp
 * Datum:				2021-05-19 10:16:45
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-19 10:16:45
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Name rückwärts ausgeben
 * Eingabe:				
 * Ausgabe:				
 * 
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	string name = "Simon Ziegler";

	for (int i = name.length() - 1; i >= 0; i--) {
		cout << name.at(i);
	}

	cout << endl;
	
	// Rückwärtsstring aufbauen

	string backwards_name;

	for (int i = name.length() - 1; i >= 0; i--) {
		backwards_name += name.at(i);
	}

	cout << "Name Rueckwaerts mit zwischen Variablen: " << backwards_name << endl;

	// Finden eines Teilstrings: find(teilstring)
	cout << endl << "ieg steht in " << name << " an der Position " << name.find("ieg") << endl;
	// Gibt den Index des gefundenen Teilstrings zurück

	string teil = "foo";
	cout << endl << teil << " steht in " << name << " an der Position " << name.find(teil) << endl;
	// Wenn der Teilstring NICHT gefunden wird, wird eine riesengroße Zahl zurück gegeben!

	/**
	 * Stringverändernde Funktionen
	 */
	// Einfügen: insert

	name.insert(6, "Michael ");
	/**
	 * Alternativ:
	 * name.insert(6, "Michael ", 0, 8); --> An Position 6 von "Michael " ab Position 0 8 Zeichen
	 */


	cout << endl << "mit Zweitnamen: " << name << endl;


	// Löschen: erase

	int anzahl_zeichen = name.find("Zieg") - name.find("icha") - 1;

	name.erase(name.find("Michael") + 1, anzahl_zeichen); // Lösche ab Position 9 anzahlZeichen viele Zeichen
	cout << endl << "nach erase: " << name << endl;

	
	// Ersetzen: replace

	name.replace(0, 5, "Herr", 0, 4);
	/**
	 * Ersetze im String name ab Position 0 insgesamt 5 Zeichen
	 * durch den Eresetzungsstring "Herr" und zwar ab Pisiton 0 insgesamt 4 Zeichen
	 * Die letzten beiden Argumente können entfallen, wenn der gesamte
	 * Ersetzungsstring verwendet wird
	 */
	cout << endl << "nach replace: " << name << endl;

	return 0;
}