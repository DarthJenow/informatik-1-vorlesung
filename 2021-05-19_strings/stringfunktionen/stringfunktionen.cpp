/*
 * Dateiname:			stringfunktionen.cpp
 * Datum:				2021-05-19 10:01:35
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-19 10:01:35
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert wichtige Stringfunktionen
 * Eingabe:				Poisitionen, Strings
 * Ausgabe:				Strings, Zeichen, Poisition, Längen
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	string name = "Simon Ziegler"; // Stringvariable name Definieren und initialisieren

	/**
	 * string ist eine Klasse (Bauplan), Variable name ist ein Individuum / Objekt der Klasse string
	 * alle Funktionen der Klase string sind an ein Individuum, das diese ausführt, gekettet
	 * hier: --> verwende Punktoperator, das heißt Funktionsaufruf wie folgt:
	 * Individuumsname.Funktionsname(Argumente)
	 * 
	 * WICHTIG
	 * Klassenfunktionen können immer direkt auf die Attributwerte des aufrufenden Individuums zugreifen
	 * 
	 * Wichtige Stringfuntionen:
	 * Länge eines Strings: length
	 */

	cout << name << "hat die Länge (Anzahl Zeichen): " << name.length() << endl;
	// Wichtig: Leerzeichen werden mitgezählt!

	string vorname = "Simon";
	cout << vorname << "hat die Laenge: " << vorname.length() << endl;

	return 0;
}