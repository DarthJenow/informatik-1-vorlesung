/*
 * Dateiname:			zeichenkette.cpp
 * Datum:				2021-06-14 08:18:16
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-14 08:18:16
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

void eingabe(char* kette, int anz);
void ausgabe(char* kette, int anz);

int main(int argc, char const *argv[]) {
	int anz = 0; // Anzahl der Zeichen

	cout << "Geben Sie die Anzahl der einzugebenden Zeichen ein: ";
	cin >> anz;

	while (anz > 0) {
		char* kette = new char[anz];
		/**
		 * new belegt zusä#tzlichen Speicher für ein Array aus char
		 * der Länge anz, die Adresse davon wird dem Zeiger
		 * kette zugewiesen
		 * Es gibt KEINEN Namen des Zusatzspeichers!
		 */

		eingabe(kette, anz); // Bekommt Adresse des Arrays und Länge

		cout << "\nIhre Zeichenkette:" << endl;
		ausgabe(kette, anz);

		cout << "\nGeben Sie die Anzahl der einzugebenden Zeichen ein: ";
		cin >> anz;

		delete[] kette;
	}
	
	return 0;
}

void eingabe(char* kette, int anz) {
	cout << "Geben Sie " << anz << " Zeichen ein: ";
	for (int i = 0; i < anz; i++) {
		cin >> kette[i];
		// cin >> *(kette + i) // alternativ
	}
}

void ausgabe (char* kette, int anz) {
	for (int i = 0; i <= anz; i++) {
		cout << kette[i];
		// cout << *(kette + i) // alternativ
	}

	cout << endl;
}