/*
 * Dateiname:			auto.h
 * Datum:				2021-06-14 08:41:09
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-14 08:41:09
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 */

#pragma once 
/**
 * verhindert Mehrfachdefinitionen der selben Klasse
 * bei mehrfachem Einbinden im selben Projekt
 */

class Auto {
	private: // Hier kommen die Attribute hin, NICHT ovn außen zugreifbar
		int leistung;
		int sitze;
		int gewicht;

	public:
	/**
	 * Hier kommen die Funktionen / Methoden hin
	 * von außen aufrufbar
	 * können die Attributwerte verändern
	 */
		Auto() {}; // Standardkonstruktor
		Auto(int ps, int s, int g);
		
		// Zugriffsfunktionen
		void setLeistung(int ps);
		void setSitze(int s);
		void setGewicht(int g);

		int getLeistung();
		int getSitze();
		int getGewicht();
		
		// weitere Funktionen
		void ausgabe();

}; // Semikolon UNBEDINGT erforderlich