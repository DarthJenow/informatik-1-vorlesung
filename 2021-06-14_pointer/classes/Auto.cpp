/*
 * Dateiname:			Auto.cpp
 * Datum:				2021-06-14 09:02:34
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-14 09:02:34
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "Auto.h"

#include <iostream>
using namespace std;

/**
 * ANMERKUNG:
 * Wir sind hier außerhalb der Klassendefinition der Klasse Auto
 */

/**
 * Konstruktor
 */
Auto::Auto(int ps, int s, int g) {
	leistung = ps;
	sitze = s;
	gewicht = g;
}

/**
 * Setzen
 */
void Auto::setLeistung (int ps) {
	leistung = ps;
}

void Auto::setSitze (int s) {
	sitze = s;
}

void Auto::setGewicht (int g) {
	gewicht = g;
}

/**
 * Lesen
 */
int Auto::getLeistung() {
	return leistung;
}

int Auto::getSitze() {
	return sitze;
}

int Auto::getGewicht() {
	return gewicht;
}

void Auto::ausgabe() {
	cout << "Leistung:\t" << leistung << " PS" << endl;
	cout << "Sitze:\t\t" << sitze << endl;
	cout << "Gewicht:\t" << gewicht << endl;
}