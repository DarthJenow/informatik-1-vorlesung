/*
 * Dateiname:			main.cpp
 * Datum:				2021-06-14 08:44:09
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-14 08:44:09
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Verwendet Klasse Auto
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

#include "Auto.h"

int main(int argc, char const *argv[]) {
	Auto Tesla; // Erzeugen eines Objektes / Individuums / Variablen vom Typ / Klasse Auto

	// Tesla.leistung = 380; // geht so nicht, Attribut ist private!
	Tesla.setLeistung(380);
	Tesla.setSitze(7);
	Tesla.setGewicht(2500);

	cout << "Unser Auto hat " << Tesla.getLeistung() << " PS, " << Tesla.getSitze() << " Sitze und wiegt " << Tesla.getGewicht() << " kg." << endl;

	Auto Lotus(200, 2, 900);
	/**
	 * Erzeugen eines 2. Objektes vom Typ Auto
	 * Definitione einer 2. Variable vom Typ Auto namens Lotus
	 * Initilaisierung über erstellten Konstruktor
	 */

	cout << "\nDaten des Tesla: " << endl;
	Tesla.ausgabe();

	cout << "\nDaten des Lotus" << endl;
	Lotus.ausgabe();
	
	return 0;
}