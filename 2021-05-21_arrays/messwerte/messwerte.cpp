/*
 * Dateiname:			messwerte.cpp
 * Datum:				2021-06-02 12:36:47
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-02 12:36:47
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechne Mittelwert und Median von Messwerten
 * Eingabe:				Messwerte, Anzahl
 * Ausgabe:				Mittelwert, Median
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Erstellen Sie ein Array der Größe 7 vom Typ double namens messwerte
 * Lesen Sie die Werte vom Benutzer ein
 * Erstellen sie eine Funktion zur Ausgabe der Messwerte und geben Sie diese aus
 * Erstellen Sie eien FUnktion mittelwert, die den Mittelwert der Messwerte berechnet und zurückgibt
 */

void ausgabe(double werte[7]);
double mittelwert(double werte[7]);
double median(double werte[7]);

int anzahl_werte;

int main(int argc, char const *argv[]) {
	double messwerte[INT16_MAX] = { 0 };

	cout << "Geben Sie die Anzahl an Messwerten ein: ";
	cin >> anzahl_werte;

	cout << "Willkommen zum Messwerte Verarbeiter. Geben Sie im folgende die" << anzahl_werte << "Messwerte ein" << endl;

	for (int i = 0; i < anzahl_werte; i++) {
		cout << i + 1 << ". Messwert: ";
		cin >> messwerte[i];
	}

	cout << "\nDeine Messdaten sind:" << endl;
	ausgabe(messwerte);

	cout << "\nDer Mittelwert der Messerte ist: " << mittelwert(messwerte) << endl;
	cout << "Der Median der Messwerte ist: " << median(messwerte) << endl;

	return 0;
}

void ausgabe(double werte[]) {
	for (int i = 0; i < anzahl_werte; i++) {
		cout << i + 1 << ". Wert = " << werte[i] << endl;
	}
}

double mittelwert(double werte[]) {
	double summe = 0.0;

	for (int i = 0; i < anzahl_werte; i++) {
		summe += werte[i];
	}

	return summe / anzahl_werte;
}

double median(double werte[]) {
	// Wert der in der Mitte einer SORTIERTEN Folge von Zahlen steht

	/**
	 * Bubblesort
	 * Finde im 1. Durchlauf den größten Wert
	 * im 2. den zweitgrößten, ...
	 */

	double tausch;

	for (int i = anzahl_werte; i > 1; i--) {
		for (int j = 0; j < i - 1; j++) {
			if (werte[j] > werte[j + 1]) {
				tausch = werte[j];
				werte[j] = werte[j + 1];
				werte[j + 1] = tausch;
			}
		}
	}

	return werte[3];
}