/*
 * Dateiname:			tauschen.cpp
 * Datum:				2021-06-05 12:59:49
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-05 12:59:49
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE:
 * Schreiben Sie eine Funktion tausche vom Typ void, der 2 Variablen vom Typ int
 * übergeben werden und die deren Inhalte tauscht
 * 
 * Lesen Sie in main 2 ganze Zahlen ein
 * Machen Sie eine Kontrollausgabe
 * Rufen Sie tausche auf
 * Machen Sie eine erneute Kontrollausgabe
 */

void tausche(int a, int b);
void tausche(int * a, int * b);

int main(int argc, char const *argv[]) {
	int a = 1;
	int b = 2;

	cout << a << '\t' << b << endl;

	tausche(a, b); // funktioniert nicht

	cout << endl << a << '\t' << b << endl;

	tausche(&a, &b); // funktioniert
	
	cout << endl << a << '\t' << b << endl;

	return 0;
}

void tausche (int a, int b) { // funktionert nicht, da nur lokale Variablen
	a += b;
	b = a - b;
	a -= b;
}

void tausche (int * a, int * b) { // funktioniert, da über Speicheradressen die "echten" Variablen angesprochen werden können
	*a += *b;
	*b = *a - *b;
	*a -= *b;
}