/*
 * Dateiname:			matrizenrechnung.cpp
 * Datum:				2021-06-04 11:57:37
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-04 11:57:37
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

void eingabe(double arr[10][10], int arrx, int arry);
void init_random(double arr[10][10]);
void ausgabe(double arr[10][10], int arrx, int arry);

void add(double a[10][10], double b[10][10], double res[10][10]);
void multiply(double a[10][10], double[10][10], double res[10][10], int ax, int aybx, int bx);

int main(int argc, char const *argv[]) {
	srand (clock()); // Zufallsgenerator initialisieren

	double a[10][10] = {};
	double b[10][10] = {};
	double res[10][10] = {};
	int ax, ay, bx, by;
	
	cout << "Wie gross soll ihre erste Matrix sein? (Spalten und Zeilen durch Leerzeichen getrennt): ";
	cin >> ax >> ay;

	cout << "Wie gross soll ihre zweite Matrix sein? (Spalten und Zeilen durch Leerzeichen getrennt): ";
	cin >> bx >> by;

	// eingabe(a, ax, aybx);
	// cout << endl;
	// eingabe(b, aybx, by);
	// cout << endl;

	init_random(a);
	init_random(b);

	cout << endl << "Die erste Matrix lautet:" << endl;
	ausgabe(a, ax, ay);
	cout << endl << "Die zweite Matrix lautet:" << endl;;
	ausgabe(b, bx, by);

	if (ax == bx && ay == by) {
		cout << endl << "Die beiden Matrizen addiert ergibt:" << endl;
		add(a, b, res);
		ausgabe(res, ax, ay);
	}
	
	if (ay == bx) {
		cout << endl << "Die beiden Matrizen multipliziert ergibt:" << endl;

		multiply(a, b, res, ax, ay, by);

		cout << endl;
		ausgabe(res, ax, by);
	}

	return 0;
}

void eingabe(double arr[10][10], int arrx, int arry) {
	for (int x = 0; x < arrx; x++) {
		cout << "Geben Sie " << arry << " Werte für die erste Zeile ein (Unterteilung mit Leerzeichen): ";
		for (int y = 0; y < arry; y++) {
			cin >> arr[x][y];
		}
	}
}

void init_random(double arr[10][10]) {
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			arr[x][y] = rand() % 1000000 / 1000.0;
		}
	}
}

void ausgabe(double arr[10][10], int arrx, int arry) {
	for (int x = 0; x < arrx; x++) {
		for (int y = 0; y < arry; y++) {
			cout << arr[x][y] << '\t';
		}
		cout << endl;
	}
}

void add(double a[10][10], double b[10][10], double res[10][10]) {
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			res[x][y] = a[x][y] + b[x][y];
		}
	}
}

void multiply(double a[10][10], double b[10][10], double res[10][10], int ax, int aybx, int by) {
	for (int x = 0; x < ax; x++) {
		for (int y = 0; y < by; y++) {
			for (int i = 0; i < aybx; i++) {
				res[x][y] += a[x][i] * b[i][y];
			}
		}
	}
}