/*
 * Dateiname:			fibo_mod.cpp
 * Datum:				2021-05-07 08:36:09
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-07 08:36:09
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Programmieren Sie folgende Funktion rekursiv und iterativ
 * f(0) = 1;
 * f(1) = 2;
 * f(n) = 2 * f(n-1) - f(n-2);
 */

int64_t f_rec(int64_t);
int64_t f_iter(int64_t);

int main(int argc, char const *argv[]) {
	for (int i = 0; i < 12; i++) {
		cout << i << " --> " << f_iter(i) << endl;
	}
	
	return 0;
}

int64_t f_rec(int64_t n) {
	if (n >= 2) {
		return 2 * f_rec(n - 1) - f_rec(n - 2);
	} else {
		return n + 1;
	}
}

int64_t f_iter (int64_t n) { // iterative: klein --> groß
	int64_t altalt, alt = 0, neu = 1;

	for (int i = 1; i <= n; i++) {
		altalt = alt;
		alt = neu;
		neu = 2 * alt - altalt;
	}

	return neu;
}