/*
 * Dateiname:			ggt.cpp
 * Datum:				2021-05-07 08:49:35
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-07 08:49:35
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

/**
 * AUFGABE
 * Programmieren Sie die Bestimmung des größten gemeinsamen Teilers rekursiv
 * 
 * Regeln:
 * ggt(a, b) = ggt(a-b, b), wenn a > b
 * ggt(a, b) = ggt(a, b-a), wenn b > a
 * ggt(a, b) = a, wenn a == b
 */

int ggt(int a, int b);

int main(int argc, char const *argv[]) {
	int a, b;

	cout << "Geben Sie zwei natürliche Zahlen ein, deren groesster gemeinsamer Teiler Sie wisen möchten: ";
	cin >> a >> b;

	if (a > 0 && b > 0) {
		cout << "Der groesste gemeinsame Teiler von " << a << " und " << b << " ist " << ggt(a, b) << endl;
	} else {
		cout << "MATH ERROR - Geben Sie zwei natürliche zahlen ein!" << endl;
	}

	return 0;
}

int ggt(int a, int b) {
	if (a > b) {
		return ggt(a - b, b);
	} else if (a < b) {
		return ggt(a, b - a);
	} else {
		return a;
	}
}