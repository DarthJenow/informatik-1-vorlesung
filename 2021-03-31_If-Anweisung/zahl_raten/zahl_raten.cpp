/*
 * Dateiname:			zahl_raten.cpp
 * Datum:				/zahl_raten.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-31 11:08:42
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

/*
 * AUFGABE
 * Lesen Sie vom Benutzer ein Zeichen ein
 * wenn es eine 7 ist, dann geben Sie aus
 * "BINGO, Zeichen getroffen!", in allen anderen Fällen
 * geben Sie aus "Leider nicht das geheime Zeichen getroffen"
 * 
 * Wenn das Zeichen ein * ist, dann geben sie aus "Ende der Vorlesung"
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	char nutzer_input;
	static char geheime_zahl = '7';

	cout << "Geben Sie ihren Tipp ab: ";
	cin >> nutzer_input;

	if (nutzer_input == geheime_zahl)
	{
		cout << "BINGO! Sie haben das geheime Zeichen erraten!" << endl;
		cout << "Das geheime Zeichen ist " << geheime_zahl << endl;
	} else if (nutzer_input == '*') {
		cout << "Ende der Vorlesung" << endl;
	} else {
		cout << "Leider haben Sie nicht das geheime Zeichen erraten";
	}
	
	return 0;
}