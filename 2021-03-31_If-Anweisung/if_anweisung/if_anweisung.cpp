/*
 * Dateiname:			if_anweisung.cpp
 * Datum:				/if_anweisung.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-31 10:34:37
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Einordner einer eingelesen zahl, ob diese kleiner gleich oder größer null ist
 * Eingabe:				Zahl
 * Ausgabe:				Kategorie
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int nutzer_zahl_input, vergleichszahl = 0;

	cout << "Geben Sie eine Zahl ein: ";
	cin >> nutzer_zahl_input;

	// Jetzt nutzer_zahl_input mit vergleichszahl testen --> unterschiedliche Ausgaben danach --> Fallunterscheidung

	if (nutzer_zahl_input <= vergleichszahl) { // fals Bedingung / Behauptung war
		cout << "Ihre Zahl ist kleiner gleich " << vergleichszahl << endl;
		cout << nutzer_zahl_input << " <= " <<  vergleichszahl << endl;

		cout << "Aber das Quadrat ist positiv!" << endl;
		cout << "(" << nutzer_zahl_input << ")^2 = " << nutzer_zahl_input * nutzer_zahl_input << endl;
	} else { // hier kommt der andere Fall / Gegenteil
		cout << "Ihre Zahl ist größer als " << vergleichszahl << endl;
		cout << nutzer_zahl_input << " > " <<  vergleichszahl << endl;
	}

	if (nutzer_zahl_input == vergleichszahl)
		cout << "BINGO, Zahl ist " << vergleichszahl << "!!!" << endl;
	
	
	/*
	 * Schreibweise if-else Allgemein:
	 * 
	 * if (Bedigung) {
	 *     // Beginn Anweisungsblock if
	 *     Anweisung1;
	 *     Anweisung2;
	 *     Anweisung3;
	 *     ...
	 * } // Ende Anweisungsblock if, KEIN ';' dahinter, wenn else folgt!!!
	 * else {
	 *     AnweisungElse1;
	 *     AnweisungElse2;
	 *     ...
	 * }; // Ende else, Semikolon ist optional
	 * 
	 * Wenn nur eine Anweisung innerhalb von if oder else bearbeitet werden soll, können die geschweiften Klammern weggelassen werden:
	 * 
	 * if (Bedingung)
	 *     Anweisung;
	 * else
	 *     AnweisungElse;
	 */

	return 0;
}