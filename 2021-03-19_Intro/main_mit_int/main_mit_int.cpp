// main_mit_int
// nichts neues, da von Beginn an "richtig" (=Linux-kompatibel gemacht)

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	cout << "Dies ist ein main-Programm mit int und return 0" << endl;
	
	return 0;

	/*
	 * Es wird eine 0 zurück gegeben, wenn das Programm richtig
	 * ausgeführt wurde
	 * return --> sofortiges Ende un RÜckgabe des Wertes, der dahinter steht
	 * return 0 sollte der letzte Befehl im main-Programm sein
	 */
}