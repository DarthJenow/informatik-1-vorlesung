/*
 * Ziel: Ausgabe folgendes Dreiecks:
 * 		  *
 * 		 ***
 * 		*****
 */

#include <iostream>
using namespace std;

// int main(int argc, char *argv[]) {
// 	cout << "  *" << endl;
// 	cout << " ***" << endl;
// 	cout << "*****" << endl;
	
// 	return 0;
// }

int main(int argc, char *argv[]) {
	cout << "HO HO HO, Eugen, meiner ist größer ;)" << endl << endl;
	
	int l = 20;

	for (int i = 1; i < l; i++) {
		for (int j = 0; j < l-i; j++) {
			cout << " ";
		}

		for (int j = 0; j < i*2-1; j++) {
			cout << "*";
		}
		cout << endl;
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < l - 1; j++) {
			cout << " ";
		}

		cout << "*" << endl;
	}
	
	return 0;4
}