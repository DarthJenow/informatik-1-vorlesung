/*
 * Dateiname:			ganzzahldivision.cpp
 * Datum:				/ganzzahldivision.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-26 08:30:38
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:	
 * Eingabe:	
 * Ausgabe:	
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int a(39), b = 5, c, rest;
	double c_double;
	
	/*
	 * Definition dreiver Variablen a, b, c
	 * Initialisieren von a und b,
	 * hier a(39) steht für a = 39,
	 * ist sowohl mit Zuweisungsoperator oder
	 * Variablennamen(Wert) möglich.
	 * Letzeres gilt  NUR  für die gleichzeitige Definition!
	 */

	c = a / b;
	rest = a % b;
	/*
	 * % gibt den Rest einer Ganzzahldivison aus
	 * heisst Modulo-Operator
	 * gitl nur bei Integer-Zahlen!
	 */

	c_double = double(a) / b;
	/*
	 * eien der int-Variablen in double umwandeln
	 * --> cast-Operator
	 */

	cout << "a / b = " << a << " / " << b << " = " << c << " Rest: " << rest << endl;

	cout << "Diesmal mit dem Ergebnis als double-Variable" << endl;
	cout << "a / b = " << a << " / " << b << " = " << c_double << endl;

	cout << "Zahlen direkt als double" << endl;
	cout << "78 / 9 = " << 78 / 9.0 << endl;

	// Was passiert, wenn aus einem double ein int gemacht wird?
	cout << "Dezibruch als int: " << int(c_double) << endl;
	// Hier werden die Zahlen nach dem Komma ABGESCHNITTEN! nicht gerundet!
	
	return 0;
}