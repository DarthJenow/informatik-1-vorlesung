/*
 * Dateiname:			kurzoperatoren.cpp
 * Datum:				/kurzoperatoren.cpp
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-03-26 09:17:45
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:	Demonstriert Operatorkurzformen von arithmetischen Operatoren
 * Eingabe:	-
 * Ausgabe:	Variablenwerte
 */

#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	float a(2.718); // a = 2.718, als float

	cout << "1. a = " << a << endl;

	a++; // Kurzform für a = a + 1

	cout << "2. a = " << a << endl;

	a *= 5; // Kurzform für a = a * 5;

	cout << "3. a = " << a << endl;

	cout << "4. a = " << a++ << endl;

	cout << "5. a = " << a << endl;
	/*
	 * FAZIT
	 * a wurde nachgelagert erhöht
	 * --> KEINE KURZFORM in cout verwenden
	 */

	cout << "6. a = " << ++a << endl;
	/*
	 * funktioniert, ist aber unüblich
	 */
	
	return 0;
}