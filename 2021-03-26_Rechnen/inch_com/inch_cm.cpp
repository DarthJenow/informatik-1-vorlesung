/*
 * filename:		inch_cm.cpp
 * path:			/inch_cm.cpp
 * author:			Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * date created:	2021-03-26 08:11:09
 * date modified:	2021-03-26 08:11:28
 * Copyright (c)	2021 Simon Ziegler
 */

#include <iostream>
using namespace std;

/*
 * AUFGABE
 * Ein benutzer soll eine Zahl in inch eingeben,
 * die Sie in cm umwandeln und ausgeben
 * 1 inch = 2.54 cm
 * Verwenden Sie Konstanten, wenn möglich
 */

/*
 * Überlegen Sie:
 * Was sind Eingaben, was sind Ausgaben?
 * Wie viele Variablen brauche ich?
 * Gibt es eine mögliche Konstante?
 * Anmerkung: KOmmazahlen immer in der Form 1.5 (mit Punkt) schreiben, auch bei Eingaben!
 */

int main(int argc, char *argv[]) {
	const double inch_in_cm = 2.54;
	double inch_laenge_eingabe;
	double cm_laenge;

	cout << "Geben sie eine Länge in inch ein: l = ";
	cin >> inch_laenge_eingabe;

	cm_laenge = inch_laenge_eingabe * inch_in_cm;

	cout << "l = " << inch_laenge_eingabe << "\" = " << cm_laenge << " cm" << endl;
	
	return 0;
}