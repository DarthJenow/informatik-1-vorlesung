/*
 * Dateiname:			pointer-and-arrays.cpp
 * Datum:				2021-06-09 10:09:08
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-06-09 10:09:08
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert Zusammenhang zwischen Arrays und Zeigern
 * Eingabe:				Arrayinhalte
 * Ausgabe:				Arrayinhalte, adresen des Arrays (der Arrayzellen)
 */

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
	int m[5] = { 11, 22, 33, 44, 55 };
	/**
	 * Definiert einen zusammenhängenden Speicherplatz aus 5 Elementen vom Typ int
	 * --> 5*4 Byte = 20 Byte Größe, Zellen hängen direkt aneindander
	 * sind mit selbem Namen, hier: m, aber unterschiedlichem Index ansprechbar
	 */

	// // Einlesen der Arraywerte
	// cout << "Geben Sie 5 ganze Zahlen ein: ";
	// for (int i = 0; i < 5; i++) {
	// 	cin >> m[i];
	// }

	cout << "\nIhre Zahlen:" << endl;
	for (int i = 0; i < 5; i++) {
		cout << m[i] << '\t';
	}
	cout << endl;

	// Frage: was bekommen wir, wenn NUR der Arrayname ausgegeben wird?

	cout << "Nur Arrayname: " << m << endl;
	cout << "Adresse des ersten Arrayelements (Index 0): " << &m[0] << endl;
	/**
	 * MERKE:
	 * Im Arraynamen ohne alles steht die ADRESSE des Arrays, das heißt die adresse des 1. Bytes des 1. Elements
	 * --> Arrayname ohne alles ist ein POINTER !!! (mit der Adresse des Arrays)
	 */

	cout << "Ausgabe des Arrayeleemnts mit Index 0 über Arrayname als Pointer: " << *m << endl;

	/**
	 * Anderer Zugriff auf Arrayelemente
	 * Ausgabe des 3. Arrayelements (Index 2)
	 */
	cout << "Ausgabe m[2] klassisch: " << m[2] << endl;
	cout << "Ausgabe m[2], Pointervariante: " << *(m + 2) << endl;
	/**
	 * Verschiebt Adresse hier um 2 ganze Arrayelemnte (hier Typ int, das heißt 8 Byte) und gibt dann von dieser
	 * ermittelten Adresse den Speicherzelleninhalt aus --> *-Operator
	 */
	cout << "Ausgabe Adresse &m[2]: " << &m[2] << endl;

	cout << "\nAusgabe aller Adressen uindd aller Arraywerte mit Pointerarithmetik" << endl;
	for (int i = 0; i < 5; i++) {
		cout << "Adresse Element " << i << ". " << (m + i) << ", Inhalt: " << *(m + i) << endl;
	}

	int* container; // Neuer Adresscontainer
	container = m; // Bekommt die adresse des Arrays zugewiesen

	cout << "\nZugriff auf die Arrayelemente über den neuen Container" << endl;
	for (int i = 0; i < 5; i++) {
		cout << *(container + i) << '\t';
	}
	cout << endl;

	cout << "\nZugriff auf die Arrayelemente über den neuen Container (mit eckigen Klammern)" << endl;
	for (int i = 0; i < 5; i++) {
		cout << container[i] << '\t';
	}
	cout << endl;

	const char* a = "┤";
	cout << "\n\n\n" << a << endl;

	return 0;
}