/*
 * Dateiname:			fibonacci.cpp
 * Datum:				2021-05-06 12:39:20
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 12:39:20
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Berechnet Größe einer kaninchenpopulation
 * Eingabe:				n / Nummer der Population
 * Ausgabe:				Anzahl Mitgleider
 */

#include <iostream>
using namespace std;

/**
 * REMINDER Fibonacci-Funtion
 * f(1) = 1
 * f(2) = 1
 * f(n) = f(n-1) + f(n-2)
 */

int64_t f_rec(int64_t);
int64_t f_iter(int64_t);

int main(int argc, char const *argv[]) {
	for (int i = 0; i < 10; i++) {
		cout << i << " --> " << f_iter(i) << endl;
	}
	
	return 0;
}

int64_t f_rec(int64_t n) {
	if (n > 2) {
		return f_rec(n-1) + f_rec(n-2);
		/**
		 * Zerlege nach Vorschrift in kleinere Teilprobleme
		 * Hier: Summe der beiden Vorgänger
		 */
	} else {
		return 1;
	}
}

int64_t f_iter (int64_t n) { // iterative: klein --> groß
	/**
	 * Problem, die letzten beiden Vorgänge rmüssen immer gespeichert werden
	 * hiert: altalt und alt
	 */
	int64_t altalt, alt = 1, neu = 1;
	/**
	 * Initilaisierung:
	 * neu ist aktuellster Fibonaiwert --> fib (2) --> 1
	 * alt, ist Vorgänger von fib(2), das heißt ist fib(1) --> 1
	 */

	for (int i = 0; i <=n; i++) { // Nächste zu berechnene Fibonacci-Zahl ist fib(3) --> i mit 3 initialisiert
		altalt = alt; // Neuer Vorvorgänger ist alter Vorgänger
		alt = neu; // Neuer Vorgänger ist letzte Fibonaccizahl
		neu = altalt + alt; // Neue Fibonaccizahl ist Summe der beiden Vorgänger
	}

	return neu;
}