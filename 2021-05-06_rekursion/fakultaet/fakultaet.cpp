/*
 * Dateiname:			fakultaet.cpp
 * Datum:				2021-05-06 11:57:15
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 11:57:15
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Zeigt die Programmierung von n! rekursiv und iterativ
 * Eingabe:				natuerliche Zahl
 * Ausgabe:				Fakultät der zahl
 */

#include <iostream>
using namespace std;


int64_t fak_rek(int64_t n);
int64_t fak_iter(int64_t n);

int main(int argc, char const *argv[]) {
	int zahl;
	// cout << "Willkommen zum Fakultaetsprogramm!" << endl;
	// cout << "Geben Sie eine natuerliche Zahl zur Berechnung der Fakultaet ein: " << endl;
	
	// cin >> zahl;
	
	// if (zahl < 0) {
	// 	cout << "MATH ERROR - Geben Sie ein natuerliche Zahl oder 0 ein!" << endl;
	// } else {
	// 	cout << zahl << "! = " << fak_iter(zahl) << endl;
	// }

	cout << fak_rek(67) << endl;

	return 0;
}

int64_t fak_rek(int64_t n) {
	int64_t res = 1;
	
	for (int i = 2; i <= n; i++) {
		res *= i;

		if (i >= 65) {
			cout << "i = " << i << " --> " << res << endl;
		}
	}

	return res;
}

int64_t fak_iter(int64_t n) {
	if (n > 1) { // Abbruchbedigung, kleinster bekannter Funktionswert
		return n * fak_iter(n - 1);
		/**
		 * n! = n * (n-1)!
		 * Funktion fak ruft sich selbst wieder auf, aber mit
		 * verändertem (verkleinertem) Wert
		 */
	} else {
		return 1; // Beende Funktion und gib 1 (kleinsten bekannten Wert zurück)
	}

	/**
	 * Richtung: groß --> klein
	 * Zerlege Aufgabe in immer kleinere Teilprobleme, bis kleinstes bekanntes erreicht
	 * Problem: Teillösungen müssen zusammengesetzt werden
	 * Kosten: Zeit und Speicherplatz
	 */
}