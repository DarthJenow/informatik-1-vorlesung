/*
 * Dateiname:			countdown.cpp
 * Datum:				2021-05-06 11:35:35
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-06 11:35:35
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Macht einen Countdown
 * Eingabe:				
 * Ausgabe:				Zahlen 10 --> 0
 */

#include <iostream>
using namespace std;

void countdown(int n);

int main(int argc, char const *argv[]) {
	countdown(10);	
	
	return 0;
}

void countdown(int n) {
	cout << n << endl;

	if ( n > 0) {
		countdown(n - 1);
	}
}