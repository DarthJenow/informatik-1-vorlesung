/*
 * Dateiname:			funktionsueberladung.cpp
 * Datum:				2021-04-29 12:51:04
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-04-29 12:51:04
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include <iostream>
using namespace std;

int secret (int a) { return a*a; }
int secret (double x) { return x * 100; }
int secret (int a, int b) { return a % b; }
double secret (int a, double x) {return a / x; }
double secret (double a, double b) { return a * b; }
double secret (double a, double b, double c) { return c - b + a; }

int main(int argc, char const *argv[]) {
	int u = 3, v = 17;
	double x = 1.5, y = 2.0, z = 6.25;

	cout << "1. secret: " << secret(v) << endl; // 1. Variante ausgewählt
	cout << "2. secret: " << secret(secret(x)) << endl; // Außen 1. Variante, Innen 2. Variante
	cout << "3. secret: " << secret(x, y, z) << endl; // 6. Variante
	cout << "4. secret: " << secret(v, u) << endl; // 3. Variante

	// cout << "5. secret: " << secret(x, u) << endl; // overload-error --> mehrere mögliche Funkionen vorhanden

	/**
	 * Auswahlkriterien für den Compiler:
	 * 1. Anzahl - notfalls Typumwandlung durch cast
	 * 2. Typ, wenn mehrere mit gleicher Anzahl vorhanden
	 * 
	 * stimmen die Typen nicht überein
	 * --> Compiler sucht diejenige Funktion, bei der er am wenigsten Typen ändern muss
	 * --> geht nur, wenn eindeutig
	 */
	
	int w = 8;
	cout << "6. secret: " << secret(w, u, v) << endl;

	return 0;
}