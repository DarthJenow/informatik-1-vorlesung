/*
 * Dateiname:			arrays.cpp
 * Datum:				2021-05-20 12:18:12
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 12:18:12
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Demonstriert Arrays
 * Eingabe:				Zahlen
 * Ausgabe:				Arrayinhalte
 */

#include <iostream>
using namespace std;

void array_ausgeben(short int f[5]);

int main(int argc, char const *argv[]) {
	short int my_first_array[5];
	/**
	 * Definiert ein Array namens my-first_array vom Typ short int mit 5 Elementen
	 * 
	 * Allgemein:
	 * Typ Arrayname[Arraylänge];
	 * 
	 * Das heißt im Speicher werden 5 nebeneinanderliegende Plätze des Datentyps,
	 * hier short int, belegt
	 * 
	 * Die Variablen haben hier EINEN Namen, aber verschiedene Index
	 * WICHTIG Index beginnnt bei 0
	 */

	// Ansprechen / Zugreifen auf einzelne Arrayelemente
	my_first_array[0] = 1;
	my_first_array[1] = 6;
	my_first_array[2] = 58;
	my_first_array[3] = -1;
	my_first_array[4] = 0;
	/**
	 * Immer Index mitnennen zum Namen!
	 * [ ] heißt Indexoperator
	 */

	// Ausgabe eines Arrays
	cout << my_first_array << endl << endl; // Adresse im Speicher

	for (int i = 0; i < 5; i++) { // Schleife durchläuft Arary von vorne nach hinten, SOLANGE i kleiner Arraylänge
		cout << my_first_array[i] << endl;
	}
	
	/**
	 * ARRAYS und Schleifen sind eng miteinander verheiratet
	 * IMMER, wenn JEDER Wert de sArrays angefasst werden muss --> Durchlaufen Array mit Schleife
	 */

	/**
	 * AUFGABE
	 * Eingabe eines Arrays
	 */

	cout << endl << "Geben Sie 5 Werte für die einzelnen Array-Zellen ein: ";

	for (int i = 0; i < 5; i++) {
		cin >> my_first_array[i];
	}

	cout << endl << "Das Array hat nun folgende Werte:" << endl;
	
	array_ausgeben(my_first_array);
	/**
	 * Bei Funktionen mit Arrays als Übergabewerte --> NUR name des Arrays,
	 * kein Typ oder Länge angeben!
	 */

	cout << endl << "Tage, Wochen, Monate" << endl;

	int tage[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	/**
	 * Initiliaiseren des Arrays bei der Defintion mittels einer Initialisierungsliste
	 * geht NUR bei der Definition
	 */

	float wochen[12];

	/**
	 * AUFGABE
	 * Berechnen Sie die Wochen pro Monat als Dezimalzahl und speichern Sie diese im array wochen
	 * Geben Sie dann die Nummer des Monats, die Anzahl seiner Tage und seiner Wochen aus
	 */

	for (int i = 0; i < 12; i++) {
		wochen[i] = tage[i] / 7.0;
	}

	for (int i = 0; i < 12; i++) {
		cout << "Der " << i << ". Monat hat " << tage[i] << " Tage und " << wochen[i] << " Wochen." << endl;
	}

	return 0;
}

void array_ausgeben(short int f[5]) {
	for (int i = 0; i < 5; i++) {
		cout << f[i] << endl;
	}
}