/*
 * Dateiname:			stringfunktionen.cpp
 * Datum:				2021-05-20 11:34:55
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * Datum erstellt:		2021-05-20 11:34:55
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				siehe Aufgabe
 * Eingabe:				String
 * Ausgabe:				String, Anzahl Vokale
 */

#include <iostream>
#include <string>
using namespace std;

/**
 * AUFGABE
 * Lesen Sie einen String vom Benutzer ein (auch Leerzeichen sollen möglich sein)
 * Zählen Sie alel Vokale (A;E;I;O;U;a;e;i;o;u) und
 * Ersetzten Sie jeden Vokal durch einen *
 * Geben Sie den veränderten String aus und die Anzahl der Vokale
 */

int main(int argc, char const *argv[]) {
	// string input_string, output_string;
	// int vokal_zahl = 0;
	
	// cout << "Geben Sie einen String ein: ";
	// getline(cin, input_string);

	// for (int i = 0; i < input_string.length(); i++) {
	// 	switch (input_string.at(i)) {
	// 		case 'A':
	// 		case 'E':
	// 		case 'I':
	// 		case 'O':
	// 		case 'U':
	// 		case 'a':
	// 		case 'e':
	// 		case 'i':
	// 		case 'o':
	// 		case 'u':
	// 			output_string += '*';
	// 			vokal_zahl++;
	// 			break;
	// 		default: // alle andere Fälle
	// 			output_string += input_string.at(i);
	// 			break;
	// 	}
	// }

	// cout << "Aus deinem String: " << input_string << " wurde: " << output_string << ". Es wurden " << vokal_zahl << " Vokale ersetzt." << endl;

	/**
	 * Version 2
	 * mit "echtem" ersetzen
	 */

	int vokal_zahl = 0;
	string input_string, output_string;

	cout << "Geben sie eine String ein: ";
	getline(cin, input_string);

	output_string = input_string;

	for (int i = 0; i < output_string.length(); i++) {
		switch (output_string.at(i)) {
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				output_string.replace(i, 1, "*");
				vokal_zahl++;
				break;
		}
	}

	cout << "Aus deinem String: " << input_string << " wurde: " << output_string << ". Es wurden " << vokal_zahl << " Vokale ersetzt." << endl;

	return 0;
}